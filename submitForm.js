//button
// const buttonSubmit = document.getElementById('fld_1889239_1');
// buttonSubmit.addEventListener('click', () => sendFormData());
const form = document.getElementById('CF5bf49ccb9928d_1');
form.addEventListener('submit', (event) => sendFormData(event) );
//inputs
const inputName = document.getElementById('fld_9014900_1');
const inputSurname = document.getElementById('fld_1727450_1');
const inputEmail = document.getElementById('fld_8004623_1');
const inputCountry = document.getElementById('fld_5703758_1');
const inputPhoneModel = document.getElementById('fld_8498340_1');
const inputSmartApp = document.getElementById('fld_1623657_1');
const inputPhoneNumber = document.getElementById('fld_6827714_1');
const inputDiscription = document.getElementById('fld_4833399_1');
const inputCheckbox = document.getElementById('fld_5416591_1');
const reEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

inputName.addEventListener('change', (event) => changeValueInput('name', event));
inputSurname.addEventListener('change', (event) => changeValueInput('surname', event));
inputEmail.addEventListener('change', (event) => changeValueInput('email', event));
inputPhoneModel.addEventListener('change', (event) => changeValueInput('phoneModel', event));
inputSmartApp.addEventListener('change', (event) => changeValueInput('smartApp', event));
inputPhoneNumber.addEventListener('change', (event) => changeValueInput('phoneNumber', event));
inputDiscription.addEventListener('change', (event) => changeValueInput('discription', event));
inputCountry.addEventListener('change', () => changeValueSelect());
//validate span
const validName = document.getElementById('parsley-id-5');
const validSurname = document.getElementById('parsley-id-7');
const validEmail = document.getElementById('parsley-id-9');
const validCountry = document.getElementById('parsley-id-11');
const valSmartApp = document.getElementById('parsley-id-13');
const validDiscription = document.getElementById('parsley-id-15');
const validCheckbox = document.getElementById('parsley-id-17');
let noValidForm = false;

const dataValidInputs = [
  { id: 'name', value: '', valid: false, span: validName, inputId: "fld_9014900_1" },
  { id: 'surname', value: '', valid: false, span: validSurname, inputId: "fld_1727450_1" },
  { id: 'email', value: '', valid: false, span: validEmail, inputId: "fld_8004623_1" },
  { id: 'smartApp', value: '', valid: false, span: valSmartApp, inputId: "fld_1623657_1" },
  { id: 'discription', value: '', valid: false, span: validDiscription, inputId: "fld_4833399_1" },
];

const dataSelect = [
  { id: 'country', value: '', valid: false, span: validCountry },
];

const dataNotValidInputs = [
  { id: 'phoneModel', value: '' },
  { id: 'phoneNumber', value: '' },
];

function changeValueInput(key, event) {

  if (['name', 'surname', 'email', 'smartApp', 'discription'].includes(key)) {
    dataValidInputs.find(elem => {
      if (elem.id === key) elem.value = event.currentTarget.value;
    })
    return;
  }

  dataNotValidInputs.find(elem => {
    if (elem.id === key) elem.value = event.currentTarget.value;
  })
}

function changeValueSelect() {
  dataSelect[0].value = inputCountry.value
}


function validateForm() {
  //validation inputs
  dataValidInputs.map(elem => {
    if (elem.id === 'email') {
      reEmail.test(String(elem.value).toLowerCase()) ? elem.valid = true : elem.valid = false
    } else {
      elem.value ? elem.valid = true : elem.valid = false
    }

    if (!elem.valid && elem.span) {
      elem.span.hidden = false;
      document.getElementById(elem.inputId).parentElement.className = 'noValid';
    } else {
      elem.span.hidden = true;
      document.getElementById(elem.inputId).parentElement.className = '';
    }
  });

  //validation select
  if (!dataSelect[0].value) {
    dataSelect[0].span.hidden = false;
    inputCountry.parentElement.className = 'noValid';
  } else {
    dataSelect[0].span.hidden = true;
    inputCountry.parentElement.className = '';
  }

  //validate checkbox
  inputCheckbox.checked ? validCheckbox.hidden = true : validCheckbox.hidden = false;

  //validation all form
  noValidForm = dataValidInputs.some(elem => !elem.valid) || !dataSelect[0].span.hidden || !inputCheckbox.checked;
}

function cleanForm() {
  inputName.value = '';
  inputSurname.value = '';
  inputEmail.value = '';
  inputCountry[0].selected = true;
  inputPhoneModel.value = '';
  inputSmartApp.value = '';
  inputPhoneNumber.value = '';
  inputDiscription.value = '';
  inputCheckbox.checked = false;
}

function sendFormData(event) {
  let sendObject = {
    'name': '',
    'surname': '',
    'email': '',
    'smartApp': '',
    'description': '',
    'country': '',
    'phoneModel': '',
    'phoneNumber': '',
  }


  validateForm();
  if (noValidForm) {
    return false;
  }

  dataValidInputs.map(elem => {
    sendObject[elem.id] = elem.value;
  });

  sendObject[dataSelect[0].id] = dataSelect[0].value;
  sendObject[dataNotValidInputs[0].id] = dataNotValidInputs[0].value;
  sendObject[dataNotValidInputs[1].id] = dataNotValidInputs[1].value;
  const alertSuccess = document.getElementById('alert-success');
  const alertDanger = document.getElementById('alert-danger');
  const spinerLoad = document.getElementById('loading');
  // (async () => {
  //   try {
  //     spinerLoad.hidden = false;
  //     let response = await fetch(`index.php`, {
  //     // let response = await fetch(`${location.protocol}${location.host}:8000/email`, {
  //       method: 'POST',
  //       // url: `${location.protocol}${location.host}:8000`,
  //       // url: `${location.protocol}${location.host}:8000`,
  //       headers: {
  //         'Content-Type': 'application/json;charset=utf-8'
  //       },
  //       body: JSON.stringify(sendObject)
  //     });
  //     if (response) spinerLoad.hidden = true;
  //     if (response.statusText === 'OK') return alertSuccess.hidden = false;
  //     alertDanger.hidden = false;
  //   } catch (error) {
  //     console.error(error);
  //   }
  // })()
}