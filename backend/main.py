from typing import Optional

from fastapi import (
    FastAPI
)
from fastapi_mail import FastMail, MessageSchema, ConnectionConfig
from pydantic import EmailStr, BaseModel
from starlette.responses import JSONResponse
from config import conf
from fastapi.middleware.cors import CORSMiddleware
origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:5000",
]
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class EmailSchema(BaseModel):
    name: str
    surname: str
    email: str
    smartApp: str
    description: str
    country: str
    phoneModel: Optional[str]
    phoneNumber: Optional[str]

def html(data: EmailSchema):
    email_text = '<table border="1" width="100%" cellpadding="5">'
    for key, value in data:
        if key and value:
            email_text += f'''
            <tr>
            <th>{key}</th>
            <th>{value}</th>
            </tr>
            '''
    email_text += '</table>'
    return email_text


@app.post("/email")
async def simple_send(
        req: EmailSchema
) -> JSONResponse:
    message = MessageSchema(
        subject="Message from spirometer.pl",
        recipients=[req.email],
        body=html(req),
        subtype="html"
    )

    fm = FastMail(conf)
    await fm.send_message(message)
    return JSONResponse(status_code=200, content={"message": "email has been sent"})
