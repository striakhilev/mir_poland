﻿<!DOCTYPE html>
<!--[if lte IE 9 ]><html class="ie lt-ie9" lang="en-GB"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en-GB">
<!--<![endif]-->

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<link rel="profile" href="xfn/11.html">
	<link rel="pingback" href="https://www.mirsmartone.com/xmlrpc.php">

	<script>(function (html) { html.className = html.className.replace(/\bno-js\b/, 'js') })(document.documentElement);</script>
	<meta name='robots' content='max-image-preview:large'>

	<!-- This site is optimized with the Yoast SEO plugin v14.0.2 - https://yoast.com/wordpress/plugins/seo/ -->
	<title>MIR SmartOne</title>
	<meta name="robots" content="index, follow">
	<meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
	<meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1">
	<link rel="canonical" href="index.htm?page_id=4049&#038;lang=uk">
	<meta property="og:locale" content="en_GB">
	<meta property="og:locale:alternate" content="it_IT">
	<meta property="og:locale:alternate" content="en_US">
	<meta property="og:locale:alternate" content="fr_FR">
	<meta property="og:locale:alternate" content="es_ES">
	<meta property="og:locale:alternate" content="de_DE">
	<meta property="og:type" content="website">
	<meta property="og:title" content="MIR SmartOne">
	<meta property="og:description"
		content="FOLLOW US DOWNLOAD QUICK GUIDE English Italiano USA Français Español Deutsch WE ARE BY YOUR SIDEAGAINST A COMMON ENEMY With the resurgence of the COVID-19 pandemic you are advised to stay at home; remote medical assistance tools become of fundamental importance. For measuring blood saturation, keeping a check on lung pathologies such as asthma or [...]">
	<meta property="og:url" content="https://www.mirsmartone.com/?page_id=4049&amp;lang=uk">
	<meta property="og:site_name" content="MIR SmartOne">
	<meta property="article:publisher" content="https://www.facebook.com/SpirometerSmartOne/">
	<meta property="article:modified_time" content="2021-01-08T09:08:16+00:00">
	<meta property="og:image" content="https://www.mirsmartone.com/wp-content/uploads/2018/10/g514.png">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:creator" content="@MIRmedical">
	<meta name="twitter:site" content="@MIRmedical">
	<script type="application/ld+json"
		class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.mirsmartone.com/?page_id=4049&lang=uk/#website","url":"https://www.mirsmartone.com/?page_id=4049&lang=uk/","name":"MIR SmartOne","description":"","potentialAction":[{"@type":"SearchAction","target":"https://www.mirsmartone.com/?page_id=4049&lang=uk/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-GB"},{"@type":"ImageObject","@id":"https://www.mirsmartone.com/?page_id=4049&lang=uk#primaryimage","inLanguage":"en-GB","url":"https://www.mirsmartone.com/wp-content/uploads/2018/10/g514.png","width":464,"height":115,"caption":"SMART ONE | MIR Spirometro tascabile personale"},{"@type":"WebPage","@id":"https://www.mirsmartone.com/?page_id=4049&lang=uk#webpage","url":"https://www.mirsmartone.com/?page_id=4049&lang=uk","name":"MIR SmartOne","isPartOf":{"@id":"https://www.mirsmartone.com/?page_id=4049&lang=uk/#website"},"primaryImageOfPage":{"@id":"https://www.mirsmartone.com/?page_id=4049&lang=uk#primaryimage"},"datePublished":"2020-12-06T15:33:38+00:00","dateModified":"2021-01-08T09:08:16+00:00","inLanguage":"en-GB","potentialAction":[{"@type":"ReadAction","target":["https://www.mirsmartone.com/?page_id=4049&lang=uk"]}]}]}</script>
	<!-- / Yoast SEO plugin. -->
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
		integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
		crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
		integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
		crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link rel='dns-prefetch' href='index-1.htm'>
	<link rel="alternate" type="application/rss+xml" title="MIR SmartOne &raquo; Feed"
		href="index.htm.rss?feed=rss2&#038;lang=uk">
	<link rel="alternate" type="application/rss+xml" title="MIR SmartOne &raquo; Comments Feed"
		href="https://www.mirsmartone.com/?feed=comments-rss2&lang=uk">
	<script type="text/javascript">
		window._wpemojiSettings = { "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/", "svgExt": ".svg", "source": { "concatemoji": "https:\/\/www.mirsmartone.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.7.1" } };
		!function (e, a, t) { var n, r, o, i = a.createElement("canvas"), p = i.getContext && i.getContext("2d"); function s(e, t) { var a = String.fromCharCode; p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0); e = i.toDataURL(); return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL() } function c(e) { var t = a.createElement("script"); t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t) } for (o = Array("flag", "emoji"), t.supports = { everything: !0, everythingExceptFlag: !0 }, r = 0; r < o.length; r++)t.supports[o[r]] = function (e) { if (!p || !p.fillText) return !1; switch (p.textBaseline = "top", p.font = "600 32px Arial", e) { case "flag": return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]); case "emoji": return !s([55357, 56424, 8205, 55356, 57212], [55357, 56424, 8203, 55356, 57212]) }return !1 }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]); t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function () { t.DOMReady = !0 }, t.supports.everything || (n = function () { t.readyCallback() }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function () { "complete" === a.readyState && t.readyCallback() })), (n = t.source || {}).concatemoji ? c(n.concatemoji) : n.wpemoji && n.twemoji && (c(n.twemoji), c(n.wpemoji))) }(window, document, window._wpemojiSettings);
	</script>
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>
	<link rel='stylesheet' id='bdt-uikit-css'
		href='wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.2' type='text/css' media='all'>
	<link rel='stylesheet' id='element-pack-site-css'
		href='wp-content/plugins/bdthemes-element-pack/assets/css/element-pack-site.css?ver=4.6.1' type='text/css'
		media='all'>
	<link rel='stylesheet' id='wp-block-library-css' href='wp-includes/css/dist/block-library/style.min.css?ver=5.7.1'
		type='text/css' media='all'>
	<link rel='stylesheet' id='cookie-law-info-css'
		href='wp-content/plugins/cookie-law-info/public/css/cookie-law-info-public.css?ver=1.9.4' type='text/css'
		media='all'>
	<link rel='stylesheet' id='cookie-law-info-gdpr-css'
		href='wp-content/plugins/cookie-law-info/public/css/cookie-law-info-gdpr.css?ver=1.9.4' type='text/css' media='all'>
	<link rel='stylesheet' id='bootstrap-css' href='wp-content/plugins/modal-for-elementor/css/bootstrap.css?ver=5.7.1'
		type='text/css' media='all'>
	<link rel='stylesheet' id='modal-popup-css' href='wp-content/plugins/modal-for-elementor/css/popup.css?ver=5.7.1'
		type='text/css' media='all'>
	<link rel='stylesheet' id='flatsome-icons-css' href='wp-content/themes/flatsome/assets/css/fl-icons.css?ver=3.3'
		type='text/css' media='all'>
	<link rel='stylesheet' id='cf-front-css'
		href='wp-content/plugins/caldera-forms/assets/build/css/caldera-forms-front.min.css?ver=1.9.2' type='text/css'
		media='all'>
	<link rel='stylesheet' id='jet-elements-css'
		href='wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.2.13' type='text/css' media='all'>
	<link rel='stylesheet' id='jet-elements-skin-css'
		href='wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.2.13' type='text/css' media='all'>
	<link rel='stylesheet' id='elementor-icons-css'
		href='wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.6.2' type='text/css'
		media='all'>
	<link rel='stylesheet' id='elementor-animations-css'
		href='wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=2.9.8' type='text/css' media='all'>
	<link rel='stylesheet' id='elementor-frontend-css'
		href='wp-content/plugins/elementor/assets/css/frontend.min.css?ver=2.9.8' type='text/css' media='all'>
	<link rel='stylesheet' id='font-awesome-5-all-css'
		href='wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min.css?ver=2.9.8' type='text/css' media='all'>
	<link rel='stylesheet' id='font-awesome-4-shim-css'
		href='wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min.css?ver=2.9.8' type='text/css'
		media='all'>
	<link rel='stylesheet' id='elementor-global-css' href='wp-content/uploads/elementor/css/global.css?ver=1620126436'
		type='text/css' media='all'>
	<link rel='stylesheet' id='elementor-post-3170-css'
		href='wp-content/uploads/elementor/css/post-3170.css?ver=1620126436' type='text/css' media='all'>
	<link rel='stylesheet' id='elementor-post-4049-css'
		href='wp-content/uploads/elementor/css/post-4049.css?ver=1620126555' type='text/css' media='all'>
	<link rel='stylesheet' id='flatsome-main-css' href='wp-content/themes/flatsome/assets/css/flatsome.css?ver=3.6.2'
		type='text/css' media='all'>
	<link rel='stylesheet' id='flatsome-style-css' href='wp-content/themes/mirsmartone-child/style.css?ver=3.6.2'
		type='text/css' media='all'>
	<link rel='stylesheet' id='google-fonts-1-css'
		href='css.css?family=Montserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CIBM+Plex+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CNunito%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.7.1'
		type='text/css' media='all'>
	<link rel='stylesheet' id='elementor-icons-shared-0-css'
		href='wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.12.0' type='text/css'
		media='all'>
	<link rel='stylesheet' id='elementor-icons-fa-brands-css'
		href='wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.12.0' type='text/css'
		media='all'>
	<link rel='stylesheet' id='elementor-icons-fa-solid-css'
		href='wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.12.0' type='text/css'
		media='all'>
	<script type='text/javascript' src='wp-includes/js/jquery/jquery.min.js?ver=3.5.1' id='jquery-core-js'></script>
	<script type='text/javascript' id='cookie-law-info-js-extra'>
		/* <![CDATA[ */
		var Cli_Data = { "nn_cookie_ids": [], "cookielist": [], "ccpaEnabled": "", "ccpaRegionBased": "", "ccpaBarEnabled": "", "ccpaType": "gdpr", "js_blocking": "", "custom_integration": "", "triggerDomRefresh": "" };
		var cli_cookiebar_settings = { "animate_speed_hide": "500", "animate_speed_show": "500", "background": "#FFF", "border": "#b1a6a6c2", "border_on": "", "button_1_button_colour": "#000", "button_1_button_hover": "#000000", "button_1_link_colour": "#fff", "button_1_as_button": "1", "button_1_new_win": "", "button_2_button_colour": "#333", "button_2_button_hover": "#292929", "button_2_link_colour": "#444", "button_2_as_button": "", "button_2_hidebar": "1", "button_3_button_colour": "#000", "button_3_button_hover": "#000000", "button_3_link_colour": "#fff", "button_3_as_button": "1", "button_3_new_win": "", "button_4_button_colour": "#000", "button_4_button_hover": "#000000", "button_4_link_colour": "#fff", "button_4_as_button": "1", "font_family": "inherit", "header_fix": "", "notify_animate_hide": "1", "notify_animate_show": "", "notify_div_id": "#cookie-law-info-bar", "notify_position_horizontal": "right", "notify_position_vertical": "bottom", "scroll_close": "", "scroll_close_reload": "", "accept_close_reload": "", "reject_close_reload": "", "showagain_tab": "", "showagain_background": "#fff", "showagain_border": "#000", "showagain_div_id": "#cookie-law-info-again", "showagain_x_position": "100px", "text": "#000", "show_once_yn": "", "show_once": "10000", "logging_on": "", "as_popup": "", "popup_overlay": "1", "bar_heading_text": "", "cookie_bar_as": "banner", "popup_showagain_position": "bottom-right", "widget_position": "left" };
		var log_object = { "ajax_url": "https:\/\/www.mirsmartone.com\/wp-admin\/admin-ajax.php" };
/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/plugins/cookie-law-info/public/js/cookie-law-info-public.js?ver=1.9.4'
		id='cookie-law-info-js'></script>
	<script type='text/javascript' src='wp-content/plugins/modal-for-elementor/js/jquery.cookie.js'
		id='jquery-cookie-js'></script>
	<script type='text/javascript' src='wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min.js?ver=2.9.8'
		id='font-awesome-4-shim-js'></script>
	<link rel="https://api.w.org/" href="index.php.json?rest_route=/">
	<link rel="alternate" type="application/json" href="index.php-1.json?rest_route=/wp/v2/pages/4049">
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php.xml?rsd">
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml">
	<meta name="generator" content="WordPress 5.7.1">
	<link rel='shortlink' href='index.htm'>
	<link rel="alternate" type="application/json+oembed"
		href="index.php-2.json?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=https%3A%2F%2Fwww.mirsmartone.com%2F%3Fpage_id%3D4049%26lang%3Duk">
	<link rel="alternate" type="text/xml+oembed"
		href="index.php.xml?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=https%3A%2F%2Fwww.mirsmartone.com%2F%3Fpage_id%3D4049%26lang%3Duk&#038;format=xml">
	<link rel="alternate" href="index.htm" hreflang="it">
	<link rel="alternate" href="index-2.htm?page_id=3984&#038;lang=en" hreflang="en-US">
	<link rel="alternate" href="index.htm?page_id=4049&#038;lang=uk" hreflang="en-GB">
	<link rel="alternate" href="index-3.htm?page_id=4041&#038;lang=fr" hreflang="fr">
	<link rel="alternate" href="index-4.htm?page_id=4058&#038;lang=es" hreflang="es">
	<link rel="alternate" href="index-5.htm?page_id=4066&#038;lang=de" hreflang="de">
	<style>
		.bg {
			opacity: 0;
			transition: opacity 1s;
			-webkit-transition: opacity 1s;
		}

		.bg-loaded {
			opacity: 1;
		}
	</style>
	<!--[if IE]><link rel="stylesheet" type="text/css" href="https://www.mirsmartone.com/wp-content/themes/flatsome/assets/css/ie-fallback.css"><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script><script>var head = document.getElementsByTagName('head')[0],style = document.createElement('style');style.type = 'text/css';style.styleSheet.cssText = ':before,:after{content:none !important';head.appendChild(style);setTimeout(function(){head.removeChild(style);}, 0);</script><script src="https://www.mirsmartone.com/wp-content/themes/flatsome/assets/libs/ie-flexibility.js"></script><![endif]-->
	<script type="text/javascript">
		WebFontConfig = {
			google: { families: ["Montserrat:regular,800", "Montserrat:regular,regular", "Nunito:regular,600", "Dancing+Script:regular,400",] }
		};
		(function () {
			var wf = document.createElement('script');
			wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})();</script>
	<link rel="icon" href="wp-content/uploads/2019/02/cropped-favicon-32x32.png" sizes="32x32">
	<link rel="icon" href="wp-content/uploads/2019/02/cropped-favicon-192x192.png" sizes="192x192">
	<link rel="apple-touch-icon" href="wp-content/uploads/2019/02/cropped-favicon-180x180.png">
	<meta name="msapplication-TileImage"
		content="https://www.mirsmartone.com/wp-content/uploads/2019/02/cropped-favicon-270x270.png">
	<style id="custom-css" type="text/css">
		:root {
			--primary-color: #f6b331;
		}

		/* Site Width */
		.header-main {
			height: 30px
		}

		#logo img {
			max-height: 30px
		}

		#logo {
			width: 30px;
		}

		.header-bottom {
			min-height: 55px
		}

		.header-top {
			min-height: 30px
		}

		.transparent .header-main {
			height: 30px
		}

		.transparent #logo img {
			max-height: 30px
		}

		.has-transparent+.page-title:first-of-type,
		.has-transparent+#main>.page-title,
		.has-transparent+#main>div>.page-title,
		.has-transparent+#main .page-header-wrapper:first-of-type .page-title {
			padding-top: 30px;
		}

		.header.show-on-scroll,
		.stuck .header-main {
			height: 30px !important
		}

		.stuck #logo img {
			max-height: 30px !important
		}

		.search-form {
			width: 96%;
		}

		.header-bg-color,
		.header-wrapper {
			background-color: rgba(255, 255, 255, 0)
		}

		.header-bottom {
			background-color: #f1f1f1
		}

		@media (max-width: 549px) {
			.header-main {
				height: 70px
			}

			#logo img {
				max-height: 70px
			}
		}

		.header-top {
			background-color: #3e3c3d !important;
		}

		/* Color */
		.accordion-title.active,
		.has-icon-bg .icon .icon-inner,
		.logo a,
		.primary.is-underline,
		.primary.is-link,
		.badge-outline .badge-inner,
		.nav-outline>li.active>a,
		.nav-outline>li.active>a,
		.cart-icon strong,
		[data-color='primary'],
		.is-outline.primary {
			color: #f6b331;
		}

		/* Color !important */
		[data-text-color="primary"] {
			color: #f6b331 !important;
		}

		/* Background */
		.scroll-to-bullets a,
		.featured-title,
		.label-new.menu-item>a:after,
		.nav-pagination>li>.current,
		.nav-pagination>li>span:hover,
		.nav-pagination>li>a:hover,
		.has-hover:hover .badge-outline .badge-inner,
		button[type="submit"],
		.button.wc-forward:not(.checkout):not(.checkout-button),
		.button.submit-button,
		.button.primary:not(.is-outline),
		.featured-table .title,
		.is-outline:hover,
		.has-icon:hover .icon-label,
		.nav-dropdown-bold .nav-column li>a:hover,
		.nav-dropdown.nav-dropdown-bold>li>a:hover,
		.nav-dropdown-bold.dark .nav-column li>a:hover,
		.nav-dropdown.nav-dropdown-bold.dark>li>a:hover,
		.is-outline:hover,
		.tagcloud a:hover,
		.grid-tools a,
		input[type='submit']:not(.is-form),
		.box-badge:hover .box-text,
		input.button.alt,
		.nav-box>li>a:hover,
		.nav-box>li.active>a,
		.nav-pills>li.active>a,
		.current-dropdown .cart-icon strong,
		.cart-icon:hover strong,
		.nav-line-bottom>li>a:before,
		.nav-line-grow>li>a:before,
		.nav-line>li>a:before,
		.banner,
		.header-top,
		.slider-nav-circle .flickity-prev-next-button:hover svg,
		.slider-nav-circle .flickity-prev-next-button:hover .arrow,
		.primary.is-outline:hover,
		.button.primary:not(.is-outline),
		input[type='submit'].primary,
		input[type='submit'].primary,
		input[type='reset'].button,
		input[type='button'].primary,
		.badge-inner {
			background-color: #f6b331;
		}

		/* Border */
		.nav-vertical.nav-tabs>li.active>a,
		.scroll-to-bullets a.active,
		.nav-pagination>li>.current,
		.nav-pagination>li>span:hover,
		.nav-pagination>li>a:hover,
		.has-hover:hover .badge-outline .badge-inner,
		.accordion-title.active,
		.featured-table,
		.is-outline:hover,
		.tagcloud a:hover,
		blockquote,
		.has-border,
		.cart-icon strong:after,
		.cart-icon strong,
		.blockUI:before,
		.processing:before,
		.loading-spin,
		.slider-nav-circle .flickity-prev-next-button:hover svg,
		.slider-nav-circle .flickity-prev-next-button:hover .arrow,
		.primary.is-outline:hover {
			border-color: #f6b331
		}

		.nav-tabs>li.active>a {
			border-top-color: #f6b331
		}

		.widget_shopping_cart_content .blockUI.blockOverlay:before {
			border-left-color: #f6b331
		}

		.woocommerce-checkout-review-order .blockUI.blockOverlay:before {
			border-left-color: #f6b331
		}

		/* Fill */
		.slider .flickity-prev-next-button:hover svg,
		.slider .flickity-prev-next-button:hover .arrow {
			fill: #f6b331;
		}

		body {
			font-size: 100%;
		}

		@media screen and (max-width: 549px) {
			body {
				font-size: 100%;
			}
		}

		body {
			font-family: "Montserrat", sans-serif
		}

		body {
			font-weight: 0
		}

		body {
			color: #3e3c3d
		}

		.nav>li>a {
			font-family: "Nunito", sans-serif;
		}

		.nav>li>a {
			font-weight: 600;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6,
		.heading-font,
		.off-canvas-center .nav-sidebar.nav-vertical>li>a {
			font-family: "Montserrat", sans-serif;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6,
		.heading-font,
		.banner h1,
		.banner h2 {
			font-weight: 800;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6,
		.heading-font {
			color: #3e3c3d;
		}

		.alt-font {
			font-family: "Dancing Script", sans-serif;
		}

		.alt-font {
			font-weight: 400 !important;
		}

		.header:not(.transparent) .header-nav.nav>li>a {
			color: #ffffff;
		}

		.header:not(.transparent) .header-nav.nav>li>a:hover,
		.header:not(.transparent) .header-nav.nav>li.active>a,
		.header:not(.transparent) .header-nav.nav>li.current>a,
		.header:not(.transparent) .header-nav.nav>li>a.active,
		.header:not(.transparent) .header-nav.nav>li>a.current {
			color: #ffffff;
		}

		.header-nav.nav-line-bottom>li>a:before,
		.header-nav.nav-line-grow>li>a:before,
		.header-nav.nav-line>li>a:before,
		.header-nav.nav-box>li>a:hover,
		.header-nav.nav-box>li.active>a,
		.header-nav.nav-pills>li>a:hover,
		.header-nav.nav-pills>li.active>a {
			color: #FFF !important;
			background-color: #ffffff;
		}

		.footer-2 {
			background-color: #3e3c3d
		}

		.absolute-footer,
		html {
			background-color: #3e3c3d
		}

		.label-new.menu-item>a:after {
			content: "New";
		}

		.label-hot.menu-item>a:after {
			content: "Hot";
		}

		.label-sale.menu-item>a:after {
			content: "Sale";
		}

		.label-popular.menu-item>a:after {
			content: "Popular";
		}

		.noValid>.form-control {
			border-bottom: 2px solid #a94442 !important;
		}

		.elementor-4049 .elementor-element.elementor-element-5b0a .caldera_forms_form .send-form {
			color: #ffffff;
			font-size: 20px;
			font-weight: 800;
			letter-spacing: 2px;
			width: 100%;
			background-color: #f6b331;
		}

		.elementor-element.elementor-element-49e3.elementor-fixed.elementor-widget.elementor-widget-image {
			width: 300px;
			height: 75px;
		}

		.elementor-element.elementor-element-49e3.elementor-fixed.elementor-widget.elementor-widget-image>.elementor-widget-container {
			margin-left: 0;
		}

		.elementor-element-377c {
			display: flex;
		}

		.elementor-element-377c>.elementor-widget-container {
			margin-left: 20px !important;
			margin-right: 0 !important;
		}

		.alert-wrapper {
			position: fixed;
			top: 0;
			left: 0;
			z-index: 100;
			width: 100%;
			height: 100%;
			overflow-x: hidden;
			overflow-y: auto;
			outline: 0;
			background-color: rgb(0 0 0 / 50%);
		}

		.alert-dialog {
			max-width: 500px;
    	margin: 1.75rem auto;
			position: relative;
   		width: auto;
		}
		.alert-content {
			position: relative;
			display: flex;
			flex-direction: column;
			width: 100%;
			pointer-events: auto;
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid rgba(0,0,0,.2);
			border-radius: .3rem;
			outline: 0;
		}
		.alert-header {
			display: flex;
			flex-shrink: 0;
			align-items: center;
			justify-content: space-between;
			padding: 1rem 1rem;
			border-bottom: 1px solid #3E3C3D;;
			border-top-left-radius: calc(.3rem - 1px);
			border-top-right-radius: calc(.3rem - 1px);
		}
		.alert-title {
			margin-bottom: 0;
			line-height: 1.5;
			color: #3E3C3D;
			font-size: 20px;
			font-weight: 900;
		}
		.alert-title > h5 {
			font-size: 1.25rem;
		}
		.alert-body {
			position: relative;
			flex: 1 1 auto;
			padding: 1rem;
		}
		.alert-body > p {
			color: #3E3C3D;
    	font-weight: 500;
	    margin-top: 0;
  	  margin-bottom: 1rem;
		}
    /* .alert {
			position: absolute;
			top: 200px;
			left: 50%;
			transform: translateX(-50%);
			z-index: 10;
			width: 300px;
			height: 300px;
			box-shadow: 5px 5px 10px rgba(0,0,0,0.5);
			display: flex;
			justify-content: center;
   		align-items: center;
		  background-color: #999;
      text-align: center;
      margin: 21px 300px 0px;
			text-align: center;
			color: black;
			font-weight: 500;
			font-size: 20px;
    }
		.alert-success {
			margin: 0;
			border-color: #54595f;
		} */
	</style>
</head>

<body
	class="home page-template page-template-page-blank-landingpage page-template-page-blank-landingpage-php page page-id-4049 lightbox nav-dropdown-has-arrow elementor-default elementor-kit-3170 elementor-page elementor-page-4049">
  <div id="wrapper">

		<div id="main" class="">

			<div data-elementor-type="wp-page" data-elementor-id="4049" class="elementor elementor-4049"
				data-elementor-settings="[]">
				<div class="elementor-inner">
					<div class="elementor-section-wrap">
						<section
							class="elementor-element elementor-element-2e53 elementor-section-content-middle elementor-section-stretched elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
							data-id="2e53" data-element_type="section"
							data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-785b elementor-column elementor-col-33 elementor-top-column"
										data-id="785b" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-377c elementor-position-right elementor-vertical-align-middle elementor-hidden-phone elementor-view-default elementor-widget elementor-widget-icon-box"
													data-id="377c" data-element_type="widget" data-widget_type="icon-box.default">
													<div class="elementor-widget-container">
														<div class="elementor-icon-box-wrapper">
															<div class="elementor-icon-box-icon">
																<a class="elementor-icon elementor-animation-" href="https://www.facebook.com/Carebits"
																	target="_blank">
																	<i aria-hidden="true" class="fab fa-facebook"></i>
																</a>
															</div>
															<div class="elementor-icon-box-icon">
																<a class="elementor-icon elementor-animation-"
																	href="https://www.instagram.com/carebits_ktg/" target="_blank">
																	<i aria-hidden="true" class="fab fa-instagram"></i>
																</a>
															</div>
															<div class="elementor-icon-box-content">
																<h5 class="elementor-icon-box-title">
																	<a href="contact/index.htm">PODĄŻAJ ZA NAMI</a>
																</h5>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-c47 elementor-column elementor-col-33 elementor-top-column"
										data-id="c47" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-197 elementor-widget elementor-widget-image"
													data-id="197" data-element_type="widget" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<img width="464" height="115" src="wp-content/uploads/2018/10/g514.png"
																class="attachment-full size-full" alt="SMART ONE | MIR Spirometro tascabile personale"
																loading="lazy"
																srcset="wp-content/uploads/2018/10/g514.png 464w, wp-content/uploads/2018/10/g514-300x74.png 300w"
																sizes="(max-width: 464px) 100vw, 464px">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-1a95 elementor-column elementor-col-33 elementor-top-column"
										data-id="1a95" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<section
													class="elementor-element elementor-element-2ffd elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="2ffd" data-element_type="section">
													<div class="elementor-container elementor-column-gap-default">

													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>

						<div id="alert-success" class="alert-wrapper" hidden="true">
							<div class="alert-dialog">
									<div class="alert-content">
										<div class="alert-header">
											<h5 class="alert-title" id="exampleModalLabel">Successfully</h5>
										</div>
										<div class="alert-body">
											<p>formularz wysłany POMYŚLNIE!</p>
										</div>
									</div>
								</div>
						</div>

						<section
							class="elementor-element elementor-element-30d7 elementor-section-stretched elementor-section-height-min-height elementor-section-items-top elementor-section-content-middle elementor-section-full_width elementor-reverse-mobile elementor-section-height-default elementor-section elementor-top-section"
							data-id="30d7" data-element_type="section" id="spirometro"
							data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;gradient&quot;}">
							<div class="elementor-container elementor-column-gap-wider">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-1f65 elementor-column elementor-col-50 elementor-top-column"
										data-id="1f65" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-27f8 elementor-widget elementor-widget-text-editor"
													data-id="27f8" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> <span style="color: #ff931e;"> JESTEŚMY PO TWOJEJ STRONIE </span> <br> PRZECIWKO WSPÓLNEMU WROGOWI </p>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-4e17 elementor-widget elementor-widget-text-editor"
													data-id="4e17" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> W związku z odrodzeniem się pandemii COVID-19 radzimy <strong> pozostać w domu</strong>; Narzędzia <strong> zdalnej pomocy medycznej </strong> mają obecnie fundamentalne znaczenie. </p>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-4d81 elementor-widget elementor-widget-text-editor"
													data-id="4d81" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> Do pomiaru <strong> saturacji krwi </strong>, kontroli patologii <strong> płuc</strong> - takie jak <strong> astma, zwłóknienie torbielowate </strong> lub po prostu monitoringu <strong> stanu płuc </strong>,
																masz specjalnego sprzymierzeńca: </p>
														</div>
													</div>
												</div>
												<div class="elementor-element elementor-element-3921 elementor-widget elementor-widget-image"
													data-id="3921" data-element_type="widget" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<img width="437" height="56"
																src="wp-content/uploads/2020/12/MIR065_002_04-SmartONE-OXI_logo_r02.png"
																class="attachment-full size-full" alt="smart one oxi" loading="lazy"
																srcset="wp-content/uploads/2020/12/MIR065_002_04-SmartONE-OXI_logo_r02.png 437w, wp-content/uploads/2020/12/MIR065_002_04-SmartONE-OXI_logo_r02-300x38.png 300w"
																sizes="(max-width: 437px) 100vw, 437px">
														</div>
													</div>
												</div>
												<div class="elementor-element elementor-element-3b16 elementor-widget elementor-widget-heading"
													data-id="3b16" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h3 class="elementor-header-title elementor-size-default"> Pierwszy pulsoksymetr z wbudowanym spirometrem oraz z nowatorskim czujnikiem dotykowym.
														</h3>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-368f elementor-column elementor-col-50 elementor-top-column"
										data-id="368f" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-49e3 elementor-fixed elementor-widget elementor-widget-image"
													data-id="49e3" data-element_type="widget"
													data-settings="{&quot;_position&quot;:&quot;fixed&quot;}" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<a href="contact/index.htm">
																<img width="385" height="145" src="wp-content/uploads/2020/12/buy-uk.png"
																	class="attachment-full size-full" alt="buy amazon" loading="lazy"
																	srcset="wp-content/uploads/2020/12/buy-uk.png 385w, wp-content/uploads/2020/12/buy-uk-300x113.png 300w"
																	sizes="(max-width: 385px) 100vw, 385px"> </a>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-425a bdt-custom-carousel-style-coverflow bdt-ep-shadow-mode-yes bdt-navigation-type-arrows elementor-widget elementor-widget-bdt-custom-carousel"
													data-id="425a" data-element_type="widget" data-widget_type="bdt-custom-carousel.default">
													<div class="elementor-widget-container">
														<div id="bdt-custom-carousel-425a"
															class="bdt-custom-carousel elementor-swiper bdt-custom-carousel-skin-default bdt-arrows-align-center"
															bdt-lightbox="toggle: .bdt-custom-carousel-lightbox-item; animation: slide;"
															data-settings="{&quot;autoplay&quot;:{&quot;delay&quot;:4000},&quot;loop&quot;:true,&quot;speed&quot;:800,&quot;slidesPerView&quot;:1,&quot;spaceBetween&quot;:1,&quot;centeredSlides&quot;:true,&quot;effect&quot;:&quot;coverflow&quot;,&quot;breakpoints&quot;:{&quot;767&quot;:{&quot;slidesPerView&quot;:2,&quot;spaceBetween&quot;:1},&quot;1023&quot;:{&quot;slidesPerView&quot;:2,&quot;spaceBetween&quot;:1}},&quot;navigation&quot;:{&quot;nextEl&quot;:&quot;#bdt-custom-carousel-425a .bdt-navigation-next&quot;,&quot;prevEl&quot;:&quot;#bdt-custom-carousel-425a .bdt-navigation-prev&quot;},&quot;pagination&quot;:{&quot;el&quot;:&quot;#bdt-custom-carousel-425a .swiper-pagination&quot;,&quot;type&quot;:&quot;&quot;,&quot;clickable&quot;:&quot;true&quot;}}">
															<div class="swiper-container">
																<div class="swiper-wrapper">
																	<div class="swiper-slide bdt-custom-carousel-item bdt-transition-toggle">
																		<div class="bdt-custom-carousel-thumbnail"
																			style="background-image: url(wp-content/uploads/2020/12/Landind-800x600-1-min.jpg)">
																		</div>


																	</div>
																	<div class="swiper-slide bdt-custom-carousel-item bdt-transition-toggle">
																		<div class="bdt-custom-carousel-thumbnail"
																			style="background-image: url(wp-content/uploads/2020/12/Landind-800x600-3-min.jpg)">
																		</div>


																	</div>
																	<div class="swiper-slide bdt-custom-carousel-item bdt-transition-toggle">
																		<div class="bdt-custom-carousel-thumbnail"
																			style="background-image: url(wp-content/uploads/2020/12/Landind-800x600-2-min.jpg)">
																		</div>


																	</div>
																</div>
															</div>



															<div class="bdt-position-z-index bdt-position-center bdt-visible@m">
																<div class="bdt-arrows-container bdt-slidenav-container">
																	<a href="" class="bdt-navigation-prev bdt-slidenav-previous bdt-icon bdt-slidenav"
																		bdt-icon="icon: chevron-left; ratio: 1.9"></a>
																	<a href="" class="bdt-navigation-next bdt-slidenav-next bdt-icon bdt-slidenav"
																		bdt-icon="icon: chevron-right; ratio: 1.9"></a>
																</div>
															</div>


														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-5cf5 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="5cf5" data-element_type="section">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-4c65 elementor-column elementor-col-100 elementor-top-column"
										data-id="4c65" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-2e39 elementor-align-center elementor-widget elementor-widget-button"
													data-id="2e39" data-element_type="widget" data-widget_type="button.default">
													<div class="elementor-widget-container">
														<div class="elementor-button-wrapper">
															<a href="#salute" class="elementor-button-link elementor-button elementor-size-sm"
																role="button">
																<span class="elementor-button-content-wrapper">
																	<span class="elementor-button-text"> czytaj więcej </span>
																</span>
															</a>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-5e96 elementor-view-default elementor-widget elementor-widget-icon"
													data-id="5e96" data-element_type="widget" data-widget_type="icon.default">
													<div class="elementor-widget-container">
														<div class="elementor-icon-wrapper">
															<div class="elementor-icon">
																<i aria-hidden="true" class="fas fa-chevron-down"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-5572 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="5572" data-element_type="section" id="salute"
							data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-21e1 elementor-column elementor-col-100 elementor-top-column"
										data-id="21e1" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-593c elementor-invisible elementor-widget elementor-widget-heading"
													data-id="593c" data-element_type="widget"
													data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}"
													data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-medium"> CZUJNIKI SMART ONE OXI: </h2>
													</div>
												</div>
												<section
													class="elementor-element elementor-element-e32 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="e32" data-element_type="section">
													<div class="elementor-container elementor-column-gap-wider">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-52c3 elementor-column elementor-col-25 elementor-inner-column"
																data-id="52c3" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-66b7 elementor-widget elementor-widget-image"
																			data-id="66b7" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="180" height="180" src="wp-content/uploads/2020/12/icon-oxi.png"
																						class="attachment-full size-full" alt="icon-oxi" loading="lazy"
																						srcset="wp-content/uploads/2020/12/icon-oxi.png 180w, wp-content/uploads/2020/12/icon-oxi-150x150.png 150w"
																						sizes="(max-width: 180px) 100vw, 180px">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-227d elementor-column elementor-col-25 elementor-inner-column"
																data-id="227d" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-3ed0 elementor-widget elementor-widget-heading"
																			data-id="3ed0" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-header-title elementor-size-default"> SPO2 </h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-6106 elementor-widget elementor-widget-nagłówek"
																			data-id="6106" data-element_type="widget" data-widget_type="header.default">
																			<div class="elementor-widget-container">
																				<h3 class="elementor-header-title elementor-size-default">  NASYCENIE KRWI TLENEM</h3>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-5139 elementor-column elementor-col-25 elementor-inner-column"
																data-id="5139" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-143f elementor-widget elementor-widget-image"
																			data-id="143f" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="180" height="180" src="wp-content/uploads/2020/12/icon-spiro.png"
																						class="attachment-full size-full" alt="" loading="lazy"
																						srcset="wp-content/uploads/2020/12/icon-spiro.png 180w, wp-content/uploads/2020/12/icon-spiro-150x150.png 150w"
																						sizes="(max-width: 180px) 100vw, 180px">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-755c elementor-column elementor-col-25 elementor-inner-column"
																data-id="755c" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-57a elementor-widget elementor-widget-heading"
																			data-id="57a" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-header-title elementor-size-default"> PEF </h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-155 elementor-widget elementor-widget-nagłówek"
																			data-id="155" data-element_type="widget" data-widget_type="header.default">
																			<div class="elementor-widget-container">
																				<h3 class="elementor-header-title elementor-size-default"> przepływ <br> szczytowy </h3>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
												<section
													class="elementor-element elementor-element-21361b7 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="21361b7" data-element_type="section">
													<div class="elementor-container elementor-column-gap-wider">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-953de67 elementor-column elementor-col-25 elementor-inner-column"
																data-id="953de67" data-element_type="column">
																<div class="elementor-column-wrap">
																	<div class="elementor-widget-wrap">
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-53eaf44 elementor-column elementor-col-25 elementor-inner-column"
																data-id="53eaf44" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-b7574f4 elementor-widget elementor-widget-heading"
																			data-id="b7574f4" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-header-title elementor-size-default"> BPM </h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-9b0dc79 elementor-widget elementor-widget-header"
																			data-id="9b0dc79" data-element_type="widget" data-widget_type="header.default">
																			<div class="elementor-widget-container">
																				<h3 class="elementor-header-title elementor-size-default"> przepływ <br> szczytowy </h3>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-1299365 elementor-column elementor-col-25 elementor-inner-column"
																data-id="1299365" data-element_type="column">
																<div class="elementor-column-wrap">
																	<div class="elementor-widget-wrap">
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-f6a748a elementor-column elementor-col-25 elementor-inner-column"
																data-id="f6a748a" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-2adfad1 elementor-widget elementor-widget-heading"
																			data-id="2adfad1" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-header-title elementor-size-default"> FEV1 </h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-6c28ef8 elementor-widget elementor-widget-header"
																			data-id="6c28ef8" data-element_type="widget" data-widget_type="header.default">
																			<div class="elementor-widget-container">
																				<h3 class="elementor-header-title elementor-size-default"> OBJĘTOŚĆ POWIETRZA
																					WYDECH <br> W JEDNĄ SEKUNDĘ </h3>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-6a2e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="6a2e" data-element_type="section" id="forza"
							data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-6b12 elementor-column elementor-col-100 elementor-top-column"
										data-id="6b12" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-1353 elementor-invisible elementor-widget elementor-widget-heading"
													data-id="1353" data-element_type="widget"
													data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}"
													data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> ZDALNA POMOC MEDYCZNA </h2>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-22a2 elementor-invisible elementor-widget elementor-widget-header"
													data-id="22a2" data-element_type="widget"
													data-settings="{& bdquo; _animation & rdquo;: & bdquo; slideInUp & rdquo;}"
													data-widget_type="header.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> W CZASIE COVID-19 </h2>
													</div>
												</div>
												<section
													class="elementor-element elementor-element-1870 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="1870" data-element_type="section">
													<div class="elementor-container elementor-column-gap-wider">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-7f86 elementor-column elementor-col-50 elementor-inner-column"
																data-id="7f86" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-61d1 elementor-widget elementor-widget-image"
																			data-id="61d1" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="670" height="281"
																						src="wp-content/uploads/2020/12/Landind-mani-1.jpg"
																						class="attachment-full size-full" alt="" loading="lazy"
																						srcset="wp-content/uploads/2020/12/Landind-mani-1.jpg 670w, wp-content/uploads/2020/12/Landind-mani-1-300x126.jpg 300w"
																						sizes="(max-width: 670px) 100vw, 670px">
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-451f elementor-widget elementor-widget-heading"
																			data-id="451f" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-heading-title elementor-size-default">1</h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-4013 elementor-widget elementor-widget-heading"
																			data-id="4013" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-header-title elementor-size-default"> CZYM JEST OKSYMETRIA
																				</h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-a4d elementor-widget elementor-widget-divider"
																			data-id="a4d" data-element_type="widget" data-widget_type="divider.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-divider">
																					<span class="elementor-divider-separator">
																					</span>
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-3d53 elementor-widget elementor-widget-text-editor"
																			data-id="3d53" data-element_type="widget" data-widget_type="text-editor.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-text-editor elementor-clearfix">
																					<p> Pulsoksymetria określa<strong> procent hemoglobiny we krwi, która
																							przenosi tlen</strong>. Jest to ważny parametr życiowy, podobnie jak ciśnienie krwi. Niski poziom tlenu, poniżej 95%, może być wczesnym wskaźnikiem by lekarz podjął działania.</p>
																					<p> <strong> Oksymetr lub saturometr</strong> kontroluje tę wartość.</p>
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-6654 elementor-view-default elementor-widget elementor-widget-icon"
																			data-id="6654" data-element_type="widget" data-widget_type="icon.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-icon-wrapper">
																					<div class="elementor-icon">
																						<i aria-hidden="true" class="fas fa-bullseye"></i>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-70c4 elementor-column elementor-col-50 elementor-inner-column"
																data-id="70c4" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-2802 elementor-widget elementor-widget-image"
																			data-id="2802" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="670" height="281" src="wp-content/uploads/2020/11/Img2.jpg"
																						class="attachment-full size-full" alt="" loading="lazy"
																						srcset="wp-content/uploads/2020/11/Img2.jpg 670w, wp-content/uploads/2020/11/Img2-300x126.jpg 300w"
																						sizes="(max-width: 670px) 100vw, 670px">
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-44 elementor-widget elementor-widget-heading"
																			data-id="44" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-heading-title elementor-size-default">2</h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-3887 elementor-widget elementor-widget-heading"
																			data-id="3887" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-header-title elementor-size-default"> CZYM JEST SPIROMETRIA
																				</h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-6a16 elementor-widget elementor-widget-divider"
																			data-id="6a16" data-element_type="widget" data-widget_type="divider.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-divider">
																					<span class="elementor-divider-separator">
																					</span>
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-2e16 elementor-widget elementor-widget-text-editor"
																			data-id="2e16" data-element_type="widget" data-widget_type="text-editor.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-text-editor elementor-clearfix">
																					<p> Spirometria to badanie<strong> oceniające czynność oddechową i zdrowie płuc; służy do sprawdzania drożności dróg oddechowych i wykrycia patologii płuc, takie jak astma i mukowiscydoza. Podobnie jak sprawdzasz temperaturę ciała za pomocą termometru, oddech sprawdzisz<strong> spirometrem </strong>.
																					</p>
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-60fc elementor-view-default elementor-widget elementor-widget-icon"
																			data-id="60fc" data-element_type="widget" data-widget_type="icon.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-icon-wrapper">
																					<div class="elementor-icon">
																						<i aria-hidden="true" class="fas fa-bullseye"></i>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
												<section
													class="elementor-element elementor-element-3371799 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="3371799" data-element_type="section">
													<div class="elementor-container elementor-column-gap-wider">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-a834605 elementor-column elementor-col-50 elementor-inner-column"
																data-id="a834605" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-a56bb2b elementor-widget elementor-widget-image"
																			data-id="a56bb2b" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="670" height="281"
																						src="wp-content/uploads/2020/12/MIR-670x281-05.jpg"
																						class="attachment-full size-full" alt="" loading="lazy"
																						srcset="wp-content/uploads/2020/12/MIR-670x281-05.jpg 670w, wp-content/uploads/2020/12/MIR-670x281-05-300x126.jpg 300w"
																						sizes="(max-width: 670px) 100vw, 670px">
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-246ba51 elementor-widget elementor-widget-heading"
																			data-id="246ba51" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-heading-title elementor-size-default">1</h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-77effaa elementor-widget elementor-widget-heading"
																			data-id="77effaa" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-header-title elementor-size-default"> ZDALNA POMOC MEDYCZNA </h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-db821d6 elementor-widget elementor-widget-divider"
																			data-id="db821d6" data-element_type="widget" data-widget_type="divider.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-divider">
																					<span class="elementor-divider-separator">
																					</span>
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-b901b3f elementor-widget elementor-widget-text-editor"
																			data-id="b901b3f" data-element_type="widget"
																			data-widget_type="text-editor.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-text-editor elementor-clearfix">
																					<p> Od samego początku<strong> sytuacji kryzysowej związanej z COVID-19
																						</strong>
																						należy unikać,<strong> o ile to możliwie, chodzenia do szpitala lub lekarza</strong>. Najlepiej korzystać z profesjonalnych rozwiązań, takich jak Smart One OXY.</p>
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-97aa95d elementor-view-default elementor-widget elementor-widget-icon"
																			data-id="97aa95d" data-element_type="widget" data-widget_type="icon.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-icon-wrapper">
																					<div class="elementor-icon">
																						<i aria-hidden="true" class="fas fa-bullseye"></i>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-e259bf8 elementor-column elementor-col-50 elementor-inner-column"
																data-id="e259bf8" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-581beca elementor-widget elementor-widget-image"
																			data-id="581beca" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="670" height="281"
																						src="wp-content/uploads/2020/12/MIR-670x281-06.jpg"
																						class="attachment-full size-full" alt="" loading="lazy"
																						srcset="wp-content/uploads/2020/12/MIR-670x281-06.jpg 670w, wp-content/uploads/2020/12/MIR-670x281-06-300x126.jpg 300w"
																						sizes="(max-width: 670px) 100vw, 670px">
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-37596f7 elementor-widget elementor-widget-heading"
																			data-id="37596f7" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-heading-title elementor-size-default">2</h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-119b398 elementor-widget elementor-widget-heading"
																			data-id="119b398" data-element_type="widget" data-widget_type="heading.default">
																			<div class="elementor-widget-container">
																				<h2 class="elementor-header-title elementor-size-default"> SMART ONE OXI </h2>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-7c40e99 elementor-widget elementor-widget-divider"
																			data-id="7c40e99" data-element_type="widget" data-widget_type="divider.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-divider">
																					<span class="elementor-divider-separator">
																					</span>
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-a364dfc elementor-widget elementor-widget-text-editor"
																			data-id="a364dfc" data-element_type="widget"
																			data-widget_type="text-editor.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-text-editor elementor-clearfix">
																					<p> Jest to prosty sposób na utrzymanie zdrowia układu krążenia i płuc -<strong> profesjonalny oksymetr z wbudowanym spirometrem</strong>, pierwszy na świecie aparat który łączy te dwie funkcje
																						<strong> z czujnikiem dotykowym </strong> do oksymetrii.
																					</p>
																				</div>
																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-f7f41a4 elementor-view-default elementor-widget elementor-widget-icon"
																			data-id="f7f41a4" data-element_type="widget" data-widget_type="icon.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-icon-wrapper">
																					<div class="elementor-icon">
																						<i aria-hidden="true" class="fas fa-bullseye"></i>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-7b1 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="7b1" data-element_type="section" id="forza"
							data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-6fd4 elementor-column elementor-col-100 elementor-top-column"
										data-id="6fd4" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-4c00 elementor-widget elementor-widget-image"
													data-id="4c00" data-element_type="widget" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<img width="437" height="56"
																src="wp-content/uploads/2020/12/MIR065_002_04-SmartONE-OXI_logo_r02.png"
																class="attachment-full size-full" alt="smart one oxi" loading="lazy"
																srcset="wp-content/uploads/2020/12/MIR065_002_04-SmartONE-OXI_logo_r02.png 437w, wp-content/uploads/2020/12/MIR065_002_04-SmartONE-OXI_logo_r02-300x38.png 300w"
																sizes="(max-width: 437px) 100vw, 437px">
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-7c87 elementor-invisible elementor-widget elementor-widget-heading"
													data-id="7c87" data-element_type="widget"
													data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}"
													data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> OKSYMETR Z WŁĄCZONYM
															SPIROMETR DO SPRAWDZANIA NASYCENIA KRWI I ZDROWIA PŁUC </h2>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-4ea9 elementor-invisible elementor-widget elementor-widget-header"
													data-id="4ea9" data-element_type="widget"
													data-settings="{& bdquo; _animation & rdquo;: & bdquo; slideInUp & rdquo;}"
													data-widget_type="header.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> W KOMFORTIE W DOMU </h2>
													</div>
												</div>
												<section
													class="elementor-element elementor-element-75c2 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="75c2" data-element_type="section">
													<div class="elementor-container elementor-column-gap-wider">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-5bab elementor-column elementor-col-50 elementor-inner-column"
																data-id="5bab" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-3c0f elementor-widget elementor-widget-image"
																			data-id="3c0f" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<a href="dp/B08BTSFNQ8.html" target="_blank">
																						<img width="536" height="681" src="wp-content/uploads/2020/12/device.png"
																							class="elementor-animation-grow-rotate attachment-full size-full" alt=""
																							loading="lazy"
																							srcset="wp-content/uploads/2020/12/device.png 536w, wp-content/uploads/2020/12/device-236x300.png 236w"
																							sizes="(max-width: 536px) 100vw, 536px"> </a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-5efe elementor-column elementor-col-50 elementor-inner-column"
																data-id="5efe" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-4bdc elementor-position-left elementor-vertical-align-middle bdt-icon-type-icon bdt-icon-effect-none elementor-invisible elementor-widget elementor-widget-bdt-advanced-icon-box"
																			data-id="4bdc" data-element_type="widget"
																			data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;}"
																			data-widget_type="bdt-advanced-icon-box.default">
																			<div class="elementor-widget-container">
																				<div class="bdt-advanced-icon-box">


																					<div class="bdt-advanced-icon-box-icon">
																						<span class="bdt-icon-wrapper">



																							<i aria-hidden="true" class="fas fa-sliders-h"></i>

																						</span>
																					</div>


																					<div class="bdt-advanced-icon-box-content">


																						<h3 class="bdt-advanced-icon-box-title">
																							<span>
																								Mierzy cztery wartości: </span>
																						</h3>





																						<div class="bdt-advanced-icon-box-description">
																							% SpO2 dla oksymetrii i tętna, przepływ szczytowry i FEV1, aby mieć płuca pod  ciągłą obserwacją.</div>

																					</div>
																				</div>



																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-3aed elementor-position-left elementor-vertical-align-middle bdt-icon-type-icon bdt-icon-effect-none elementor-invisible elementor-widget elementor-widget-bdt-advanced-icon-box"
																			data-id="3aed" data-element_type="widget"
																			data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;}"
																			data-widget_type="bdt-advanced-icon-box.default">
																			<div class="elementor-widget-container">
																				<div class="bdt-advanced-icon-box">


																					<div class="bdt-advanced-icon-box-icon">
																						<span class="bdt-icon-wrapper">



																							<i aria-hidden="true" class="fas fa-piggy-bank"></i>

																						</span>
																					</div>


																					<div class="bdt-advanced-icon-box-content">


																						<h3 class="bdt-advanced-icon-box-title">
																							<span>
																								Zaawansowana technologia </span>
																						</h3>





																						<div class="bdt-advanced-icon-box-description">

																						za skromną cenę i gwarancję firmy pracującej od ponad 25 lat w branży medycznej.</div>

																					</div>
																				</div>



																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-68bb elementor-position-left elementor-vertical-align-middle bdt-icon-type-icon bdt-icon-effect-none elementor-invisible elementor-widget elementor-widget-bdt-advanced-icon-box"
																			data-id="68bb" data-element_type="widget"
																			data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;}"
																			data-widget_type="bdt-advanced-icon-box.default">
																			<div class="elementor-widget-container">
																				<div class="bdt-advanced-icon-box">


																					<div class="bdt-advanced-icon-box-icon">
																						<span class="bdt-icon-wrapper">



																							<i aria-hidden="true" class="fas fa-user-plus"></i>

																						</span>
																					</div>


																					<div class="bdt-advanced-icon-box-content">


																						<h3 class="bdt-advanced-icon-box-title">
																							<span>
																								Jest bardzo łatwy w użyciu </span>
																						</h3>





																						<div class="bdt-advanced-icon-box-description">

																						dla każdego od 5 lat do 93 lat oraz dla członków tej samej rodziny; jest bardzo łatwy do czyszczenia. </div>

																					</div>
																				</div>



																			</div>
																		</div>
																		<div
																			class="elementor-element elementor-element-84 elementor-position-left elementor-vertical-align-middle bdt-icon-type-icon bdt-icon-effect-none elementor-invisible elementor-widget elementor-widget-bdt-advanced-icon-box"
																			data-id="84" data-element_type="widget"
																			data-settings="{&quot;_animation&quot;:&quot;fadeInRight&quot;}"
																			data-widget_type="bdt-advanced-icon-box.default">
																			<div class="elementor-widget-container">
																				<div class="bdt-advanced-icon-box">


																					<div class="bdt-advanced-icon-box-icon">
																						<span class="bdt-icon-wrapper">



																							<i aria-hidden="true" class="fab fa-bluetooth-b"></i>

																						</span>
																					</div>


																					<div class="bdt-advanced-icon-box-content">


																						<h3 class="bdt-advanced-icon-box-title">
																							<span>
																								W zestawie aplikacja APP MIR Smart One </span>
																						</h3>





																						<div class="bdt-advanced-icon-box-description">

																						aby przeglądać swoje wyniki w czasie rzeczywistym na smartfonie i je udostępniać. </div>

																					</div>
																				</div>



																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-3402 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="3402" data-element_type="section" id="app"
							data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-2a4c elementor-column elementor-col-50 elementor-top-column"
										data-id="2a4c" data-element_type="column">
										<div class="elementor-column-wrap">
											<div class="elementor-widget-wrap">
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-1e0f elementor-column elementor-col-50 elementor-top-column"
										data-id="1e0f" data-element_type="column"
										data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-4e92 elementor-widget elementor-widget-image"
													data-id="4e92" data-element_type="widget" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<img width="670" height="281"
																src="wp-content/uploads/2020/04/MIR-ONEpage_CONDIVISIONE.jpg"
																class="attachment-full size-full" alt="app condivisione" loading="lazy"
																srcset="wp-content/uploads/2020/04/MIR-ONEpage_CONDIVISIONE.jpg 670w, wp-content/uploads/2020/04/MIR-ONEpage_CONDIVISIONE-300x126.jpg 300w"
																sizes="(max-width: 670px) 100vw, 670px">
														</div>
													</div>
												</div>
												<div class="elementor-element elementor-element-1d6c elementor-widget elementor-widget-heading"
													data-id="1d6c" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> PRZEJDŹ TEST GDZIEKOLWIEK JESTEŚ
															I PODZIEL WYNIKIEM
														</h2>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-73f1 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="73f1" data-element_type="section"
							data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-5025 elementor-column elementor-col-50 elementor-top-column"
										data-id="5025" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-4c42 elementor-widget elementor-widget-image"
													data-id="4c42" data-element_type="widget" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<a href="dp/B08BTSFNQ8.html" target="_blank">
																<img width="623" height="680"
																	src="wp-content/uploads/2020/12/MIR065__SmartOne_OXI_Pack03-min-1-1.png"
																	class="elementor-animation-grow attachment-full size-full" alt="pack" loading="lazy"
																	srcset="wp-content/uploads/2020/12/MIR065__SmartOne_OXI_Pack03-min-1-1.png 623w, wp-content/uploads/2020/12/MIR065__SmartOne_OXI_Pack03-min-1-1-275x300.png 275w"
																	sizes="(max-width: 623px) 100vw, 623px"> </a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-14b0 elementor-column elementor-col-50 elementor-top-column"
										data-id="14b0" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-dc7 elementor-widget elementor-widget-heading"
													data-id="dc7" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> KUP SMART ONE ON </h2>
													</div>
												</div>
												<section
													class="elementor-element elementor-element-594 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="594" data-element_type="section">
													<div class="elementor-container elementor-column-gap-default">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-7765 elementor-column elementor-col-50 elementor-inner-column"
																data-id="7765" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-2b74 elementor-widget elementor-widget-image"
																			data-id="2b74" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="1014" height="312"
																						src="wp-content/uploads/2018/10/Amazon-Logo2.png"
																						class="attachment-full size-full"
																						alt="SMART ONE | MIR Spirometro tascabile personale" loading="lazy"
																						srcset="wp-content/uploads/2018/10/Amazon-Logo2.png 1014w, wp-content/uploads/2018/10/Amazon-Logo2-300x92.png 300w, wp-content/uploads/2018/10/Amazon-Logo2-768x236.png 768w"
																						sizes="(max-width: 1014px) 100vw, 1014px">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-7a22 elementor-column elementor-col-50 elementor-inner-column"
																data-id="7a22" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-6a96 elementor-mobile-align-center elementor-widget elementor-widget-button"
																			data-id="6a96" data-element_type="widget" data-widget_type="button.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-button-wrapper">
																					<a href="dp/B08BTSFNQ8.html" target="_blank"
																						class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow"
																						role="button">
																						<span class="elementor-button-content-wrapper">
																							<span class="elementor-button-text"> KLIKNIJ TUTAJ </span>
																						</span>
																					</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-6c43 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="6c43" data-element_type="section">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-a99 elementor-column elementor-col-50 elementor-top-column"
										data-id="a99" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-5431 bdt-custom-carousel-style-coverflow bdt-navigation-type-none elementor-widget elementor-widget-bdt-custom-carousel"
													data-id="5431" data-element_type="widget" data-widget_type="bdt-custom-carousel.default">
													<div class="elementor-widget-container">
														<div id="bdt-custom-carousel-5431"
															class="bdt-custom-carousel elementor-swiper bdt-custom-carousel-skin-default"
															bdt-lightbox="toggle: .bdt-custom-carousel-lightbox-item; animation: slide;"
															data-settings="{&quot;autoplay&quot;:{&quot;delay&quot;:3700},&quot;loop&quot;:true,&quot;speed&quot;:500,&quot;slidesPerView&quot;:1,&quot;spaceBetween&quot;:20,&quot;centeredSlides&quot;:true,&quot;effect&quot;:&quot;coverflow&quot;,&quot;breakpoints&quot;:{&quot;767&quot;:{&quot;slidesPerView&quot;:2,&quot;spaceBetween&quot;:20},&quot;1023&quot;:{&quot;slidesPerView&quot;:3,&quot;spaceBetween&quot;:20}},&quot;navigation&quot;:{&quot;nextEl&quot;:&quot;#bdt-custom-carousel-5431 .bdt-navigation-next&quot;,&quot;prevEl&quot;:&quot;#bdt-custom-carousel-5431 .bdt-navigation-prev&quot;},&quot;pagination&quot;:{&quot;el&quot;:&quot;#bdt-custom-carousel-5431 .swiper-pagination&quot;,&quot;type&quot;:&quot;&quot;,&quot;clickable&quot;:&quot;true&quot;}}">
															<div class="swiper-container">
																<div class="swiper-wrapper">
																	<div class="swiper-slide bdt-custom-carousel-item bdt-transition-toggle">
																		<div class="bdt-custom-carousel-thumbnail"
																			style="background-image: url(wp-content/uploads/2020/12/spiro-app-min.png)"></div>


																	</div>
																	<div class="swiper-slide bdt-custom-carousel-item bdt-transition-toggle">
																		<div class="bdt-custom-carousel-thumbnail"
																			style="background-image: url(wp-content/uploads/2020/11/4.png)"></div>


																	</div>
																	<div class="swiper-slide bdt-custom-carousel-item bdt-transition-toggle">
																		<div class="bdt-custom-carousel-thumbnail"
																			style="background-image: url(wp-content/uploads/2020/11/5.png)"></div>


																	</div>
																	<div class="swiper-slide bdt-custom-carousel-item bdt-transition-toggle">
																		<div class="bdt-custom-carousel-thumbnail"
																			style="background-image: url(wp-content/uploads/2020/11/6.png)"></div>


																	</div>
																</div>
															</div>





														</div>
													</div>
												</div>
												<section
													class="elementor-element elementor-element-7d elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="7d" data-element_type="section">
													<div class="elementor-container elementor-column-gap-default">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-74b7 elementor-column elementor-col-25 elementor-inner-column"
																data-id="74b7" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-2e04 elementor-widget elementor-widget-image"
																			data-id="2e04" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="320" height="326"
																						src="wp-content/uploads/2018/09/logoIosAndroid.png"
																						class="attachment-full size-full"
																						alt="SMART ONE | MIR Spirometro tascabile personale" loading="lazy"
																						srcset="wp-content/uploads/2018/09/logoIosAndroid.png 320w, wp-content/uploads/2018/09/logoIosAndroid-294x300.png 294w"
																						sizes="(max-width: 320px) 100vw, 320px">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-6a0b elementor-column elementor-col-25 elementor-inner-column"
																data-id="6a0b" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-1048 elementor-widget elementor-widget-image"
																			data-id="1048" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="800" height="208"
																						src="wp-content/uploads/2018/10/bluetooth-logo.jpg"
																						class="attachment-full size-full"
																						alt="SMART ONE | MIR Spirometro tascabile personale" loading="lazy"
																						srcset="wp-content/uploads/2018/10/bluetooth-logo.jpg 800w, wp-content/uploads/2018/10/bluetooth-logo-300x78.jpg 300w, wp-content/uploads/2018/10/bluetooth-logo-768x200.jpg 768w"
																						sizes="(max-width: 800px) 100vw, 800px">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-7221 elementor-column elementor-col-25 elementor-inner-column"
																data-id="7221" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-250b elementor-widget elementor-widget-image"
																			data-id="250b" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<a href="store/apps/details.html?id=com.spirometry.smartone.smartone&amp;hl=it"
																						target="_blank">
																						<img width="160" height="50"
																							src="wp-content/uploads/2018/09/BadgeGoogle.png"
																							class="elementor-animation-grow attachment-full size-full"
																							alt="SMART ONE | MIR Spirometro tascabile personale" loading="lazy"> </a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-a64 elementor-column elementor-col-25 elementor-inner-column"
																data-id="a64" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-4bcf elementor-widget elementor-widget-image"
																			data-id="4bcf" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<a href="it/app/mir-smart-one/id1010664940.html?mt=8" target="_blank">
																						<img width="160" height="50" src="wp-content/uploads/2018/09/BadgeiOS.png"
																							class="elementor-animation-grow attachment-full size-full"
																							alt="SMART ONE | MIR Spirometro tascabile personale" loading="lazy"> </a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-3846 elementor-column elementor-col-50 elementor-top-column"
										data-id="3846" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-613a elementor-invisible elementor-widget elementor-widget-heading"
													data-id="613a" data-element_type="widget"
													data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}"
													data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> APP </h2>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-fe6 elementor-invisible elementor-widget elementor-widget-heading"
													data-id="fe6" data-element_type="widget"
													data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}"
													data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> MIR SMARTONE </h2>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-77e5 elementor-widget elementor-widget-text-editor"
													data-id="77e5" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> Spirometr SMART ONE łączy się z aplikacją <strong> MIR SMART ONE </strong>, która pozwala na <strong> niezależne monitorowanie stanu płuc </strong>
															przed kontaktem z lekarzem. Aplikacja MIR Smart One umożliwia: </p>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-d0 elementor-widget elementor-widget-text-editor"
													data-id="d0" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> <strong> <span style="color: #f2990b;"> \ </span> </strong> Wyświetlanie <strong> wyniku testu </strong> w czasie rzeczywistym, bezpośrednio na smartfonie lub tablecie przez Bluetooth. </p>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-c86 elementor-widget elementor-widget-edytor tekstu"
													data-id="c86" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> <strong> <span style="color: #f2990b;"> \ </span> </strong>Zapisywać objawy, <strong>
															robić notatki i zapisywać wyniki </strong> w miarę upływu czasu na smartfonie /
															tablecie, śledzić swoje zdrowie dzień po dniu. </p>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-7a73 elementor-widget elementor-widget-text-editor"
													data-id="7a73" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> <strong> <span style="color: #f2990b;"> \ </span>Udostępniać wyniki badań </strong>
															swojemu lekarzowi lub komukolwiek chcesz, korzystając z poczty e-mail, WhatsApp, SMS-ów i innych aplikacji, bezpośrednio ze smartfona / tabletu. </p>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-3034 elementor-widget elementor-widget-edytor tekstu"
													data-id="3034" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> <strong> <span style="color: #f2990b;"> \ </span> </strong> Łatwy odczyt wyników testów,
																w tym <strong> sygnalizacja świetlna Peak Flow </strong> (ZIELONA = OK, ŻÓŁTA = ostrzeżenie, CZERWONA = do lekarza). </p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-25aa elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="25aa" data-element_type="section">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-662b elementor-column elementor-col-100 elementor-top-column"
										data-id="662b" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-5321 elementor-widget elementor-widget-image"
													data-id="5321" data-element_type="widget" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<img width="92" height="70" src="wp-content/uploads/2019/03/g98.png"
																class="attachment-full size-full" alt="" loading="lazy">
														</div>
													</div>
												</div>
												<section
													class="elementor-element elementor-element-6413 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="6413" data-element_type="section">
													<div class="elementor-container elementor-column-gap-default">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-5969 elementor-column elementor-col-33 elementor-inner-column"
																data-id="5969" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-1fb3 elementor-widget elementor-widget-image"
																			data-id="1fb3" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="250" height="185"
																						src="wp-content/uploads/2019/03/MIR-ONEpage_CONDIVISIONE-1.png"
																						class="attachment-full size-full" alt="" loading="lazy">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-1183 elementor-column elementor-col-33 elementor-inner-column"
																data-id="1183" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-2533 elementor-widget elementor-widget-image"
																			data-id="2533" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="250" height="185"
																						src="wp-content/uploads/2019/03/MIR-ONEpage_CONDIVISIONE2-1.png"
																						class="attachment-full size-full" alt="" loading="lazy">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-101 elementor-column elementor-col-33 elementor-inner-column"
																data-id="101" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-6e3c elementor-widget elementor-widget-image"
																			data-id="6e3c" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="250" height="185"
																						src="wp-content/uploads/2019/03/MIR-ONEpage_CONDIVISIONE3-1.png"
																						class="attachment-full size-full" alt="" loading="lazy">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-35dd elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="35dd" data-element_type="section">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-3285 elementor-column elementor-col-100 elementor-top-column"
										data-id="3285" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-7ecb elementor-invisible elementor-widget elementor-widget-heading"
													data-id="7ecb" data-element_type="widget"
													data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}"
													data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h2 class="elementor-header-title elementor-size-default"> certyfikowana jakość </h2>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-31b9 elementor-invisible elementor-widget elementor-widget-heading"
													data-id="31b9" data-element_type="widget"
													data-settings="{&quot;_animation&quot;:&quot;slideInUp&quot;}"
													data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h6 class="elementor-header-title elementor-size-default"> globalnie </h6>
													</div>
												</div>
												<section
													class="elementor-element elementor-element-263a elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="263a" data-element_type="section">
													<div class="elementor-container elementor-column-gap-default">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-468e elementor-column elementor-col-25 elementor-inner-column"
																data-id="468e" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-e6c elementor-widget elementor-widget-image"
																			data-id="e6c" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="157" height="144" src="wp-content/uploads/2020/11/logo4.jpg"
																						class="attachment-full size-full" alt="" loading="lazy">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-4d5f elementor-column elementor-col-25 elementor-inner-column"
																data-id="4d5f" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-6764 elementor-widget elementor-widget-image"
																			data-id="6764" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="410" height="215" src="wp-content/uploads/2020/12/ers.jpg"
																						class="attachment-full size-full" alt="ers" loading="lazy"
																						srcset="wp-content/uploads/2020/12/ers.jpg 410w, wp-content/uploads/2020/12/ers-300x157.jpg 300w"
																						sizes="(max-width: 410px) 100vw, 410px">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-1524 elementor-column elementor-col-25 elementor-inner-column"
																data-id="1524" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-1565 elementor-widget elementor-widget-image"
																			data-id="1565" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="410" height="215" src="wp-content/uploads/2020/12/ATS.jpg"
																						class="attachment-full size-full" alt="ats" loading="lazy"
																						srcset="wp-content/uploads/2020/12/ATS.jpg 410w, wp-content/uploads/2020/12/ATS-300x157.jpg 300w"
																						sizes="(max-width: 410px) 100vw, 410px">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div
																class="elementor-element elementor-element-13f elementor-column elementor-col-25 elementor-inner-column"
																data-id="13f" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-7d69 elementor-widget elementor-widget-image"
																			data-id="7d69" data-element_type="widget" data-widget_type="image.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-image">
																					<img width="294" height="163" src="wp-content/uploads/2020/11/logo3.jpg"
																						class="attachment-full size-full" alt="" loading="lazy">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-44af elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="44af" data-element_type="section" id="azienda"
							data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-53a5 elementor-column elementor-col-50 elementor-top-column"
										data-id="53a5" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-2560 elementor-widget elementor-widget-image"
													data-id="2560" data-element_type="widget" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<img width="464" height="115" src="wp-content/uploads/2018/10/g514.png"
																class="attachment-full size-full" alt="SMART ONE | MIR Spirometro tascabile personale"
																loading="lazy"
																srcset="wp-content/uploads/2018/10/g514.png 464w, wp-content/uploads/2018/10/g514-300x74.png 300w"
																sizes="(max-width: 464px) 100vw, 464px">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-5bcb elementor-column elementor-col-50 elementor-top-column"
										data-id="5bcb" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-561d elementor-widget elementor-widget-text-editor"
													data-id="561d" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> Aplikacje SMART ONE i MIR SMART ONE zostały opracowane przez MIR, Medical
																International Research, światowy lider w zakresie innowacji i know-how w spirometrii,
																oksymetria i telemedycyna. </p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-1851 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="1851" data-element_type="section">
							<div class="elementor-container elementor-column-gap-no">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-78ae elementor-column elementor-col-50 elementor-top-column"
										data-id="78ae" data-element_type="column">
										<div class="elementor-column-wrap">
											<div class="elementor-widget-wrap">
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-172c elementor-invisible elementor-column elementor-col-50 elementor-top-column"
										data-id="172c" data-element_type="column"
										data-settings="{&quot;animation&quot;:&quot;slideInDown&quot;}">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-3b60 elementor-widget elementor-widget-text-editor"
													data-id="3b60" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-4d08 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="4d08" data-element_type="section"
							data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-5acc elementor-column elementor-col-100 elementor-top-column"
										data-id="5acc" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-16b6 elementor-widget elementor-widget-heading"
													data-id="16b6" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h3 class="elementor-header-title elementor-size-default"> Pomoc techniczna </h3>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-17b9 elementor-widget elementor-widget-text-editor"
													data-id="17b9" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> Posiadasz Smart One i potrzebujesz pomocy? <br> WYPEŁNIJ FORMULARZ </p>
														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-5b0a elementor-widget elementor-widget-bdt-caldera-forms"
													data-id="5b0a" data-element_type="widget" data-widget_type="bdt-caldera-forms.default">
													<div class="elementor-widget-container">
														<div class="caldera-grid" id="caldera_form_1" data-cf-ver="1.9.2"
															data-cf-form-id="CF5bf49ccb9928d">
															<!-- loader -->
															<div id="loading" class="alert alert-info" role="alert" hidden="true">
																<div class="d-flex justify-content-center" >
																	<div class="spinner-border" role="status">
																		<span class="visually-hidden">Loading...</span>
																	</div>
																</div>
															</div>
	
															<!-- alerts -->
															<!-- <div id="alert-success" class="alert alert-success" role="alert" hidden="true">
																formularz wysłany POMYŚLNIE!t!
															</div>
															<div id="alert-danger" class="alert alert-danger" role="alert" hidden="true">
																BŁĄD: spróbuj ponownie lub skontaktuj się z nami telefonicznie!
															</div> -->
															<!-- FORM	-->
															<form action="./mail.php" method="post" class="CF5bf49ccb9928d caldera_forms_form cfajax-trigger" id="CF5bf49ccb9928d_1">
																<div id="CF5bf49ccb9928d_1-row-1" class="row  first_row">
																	<div class="col-sm-6  first_col">
																		<!-- Name -->
																		<div data-field-wrapper="fld_9014900" class="form-group" id="fld_9014900_1-wrap">
																			<label id="fld_9014900Label" for="fld_9014900_1"
																				class="control-label-screen-reader-text sr-only"> Nazwa </label>
																			<div class="">
																				<input placeholder="Nazwa *" type="text" class=" form-control"
																					id="fld_9014900_1" name="name" value="">
																			</div>
																			<span class="help-block caldera_ajax_error_block filled" id="parsley-id-5"
																				aria-live="polite" hidden="true"><span class="parsley-required"
																					style="color: #a94442">Ta wartość jest
																					wymagana.</span></span>
																		</div>
																		<!-- Surname -->
																		<div data-field-wrapper="fld_1727450" class="form-group" id="fld_1727450_1-wrap">
																			<label id="fld_1727450Label" for="fld_1727450_1"
																				class="control-label-screen-reader-text sr-only"> Nazwisko </label>
																			<div class="">
																				<input placeholder="Nazwisko *" type="text" class=" form-control"
																					id="fld_1727450_1" name="surname" value="">
																			</div>
																			<span class="help-block caldera_ajax_error_block filled" id="parsley-id-7"
																				aria-live="polite" hidden="true"><span class="parsley-required"
																					style="color: #a94442">Ta wartość jest
																					wymagana.</span></span>
																		</div>
																		<!-- email -->
																		<div data-field-wrapper="fld_8004623" class="form-group" id="fld_8004623_1-wrap">
																			<label id="fld_8004623Label" for="fld_8004623_1"
																				class="control-label-screen-reader-text sr-only" style="color: #a94442"> E-mail
																			</label>
																			<div class="">
																				<input placeholder="Email *" type="email" class=" form-control"
																					id="fld_8004623_1" name="email" value="">
																			</div>
																			<span class="help-block caldera_ajax_error_block filled" id="parsley-id-9"
																				aria-live="polite" hidden="true"><span class="parsley-required"
																					style="color: #a94442">Ta wartość
																					powinien być prawidłowym adresem e-mail: „john@gmail.com”.</span></span>
																		</div>
																		<!-- country select -->
																		<div data-field-wrapper="fld_5703758" class="form-group" id="fld_5703758_1-wrap">
																			<label id="fld_5703758Label" for="fld_5703758_1"
																				class="control-label-screen-reader-text sr-only"> Kraj </label>
																			<div class="">
																				<select name="country" value="" class="form-control" id="fld_5703758_1">
																					<option value=""> Kraj * </option>
																					<option value="Afghanistan" data-calc-value="Afghanistan">
																						Afganistan </option>
																					<option value="Wyspy Alandzkie" data-calc-value="Wyspy Alandzkie">
																						Wyspy Alandzkie </option>
																					<option value="Albania" data-calc-value="Albania">
																						Albania </option>
																					<option value="Algeria" data-calc-value="Algeria">
																						Algieria </option>
																					<option value="American Samoa" data-calc-value="American Samoa">
																						Samoa Amerykańskie </option>
																					<option value="Andorra" data-calc-value="Andorra">
																						Andora </option>
																					<option value="Angola" data-calc-value="Angola">
																						Angola </option>
																					<option value="Anguilla" data-calc-value="Anguilla">
																						Anguilla </option>
																					<option value="Antarctica" data-calc-value="Antarctica">
																						Antarktyda </option>
																					<option value="Antigua i Barbuda" data-calc-value="Antigua i Barbuda">
																						Antigua i Barbuda </option>
																					<option value="Argentina" data-calc-value="Argentina">
																						Argentyna </option>
																					<option value="Armenia" data-calc-value="Armenia">
																						Armenia </option>
																					<option value="Aruba" data-calc-value="Aruba">
																						Aruba </option>
																					<option value="Australia" data-calc-value="Australia">
																						Australia </option>
																					<option value="Austria" data-calc-value="Austria">
																						Austria </option>
																					<option value="Azerbejdżan" data-calc-value="Azerbejdżan">
																						Azerbejdżan </option>
																					<option value="Bahamas" data-calc-value="Bahamas">
																						Bahamy </option>
																					<option value="Bahrain" data-calc-value="Bahrain">
																						Bahrajn </option>
																					<option value="Bangladesh" data-calc-value="Bangladesh">
																						Bangladesz </option>
																					<option value="Barbados" data-calc-value="Barbados">
																						Barbados </option>
																					<option value="Belarus" data-calc-value="Belarus">
																						Białoruś </option>
																					<option value="Belgium" data-calc-value="Belgium">
																						Belgia </option>
																					<option value="Belize" data-calc-value="Belize">
																						Belize </option>
																					<option value="Benin" data-calc-value="Benin">
																						Benin </option>
																					<option value="Bermudy" data-calc-value="Bermuda">
																						Bermudy </option>
																					<option value="Bhutan" data-calc-value="Bhutan">
																						Bhutan </option>
																					<option value="Boliwia, Wielonarodowe państwo"
																						data-calc-value="Boliwia, wielonarodowe państwo">
																						Boliwia, Wielonarodowe Państwo </option>
																					<wartość opcji=„Bonaire, Sint Eustatius i Saba”
																						data-calc-value="Bonaire, Sint Eustatius and Saba">
																						Bonaire, Sint Eustatius i Saba </option>
																						<option value="Bośnia i Hercegowina" data-calc-value="Bośnia i Hercegowina">
																							Bośnia i Hercegowina </option>
																						<option value="Botswana" data-calc-value="Botswana">
																							Botswana </option>
																						<option value="Wyspa Bouveta" data-calc-value="Wyspa Bouveta">
																							Wyspa Bouveta </option>
																						<option value="Brazil" data-calc-value="Brazil">
																							Brazylia </option>
																						<option value="Brytyjskie Terytorium Oceanu Indyjskiego"
																							data-calc-value="Brytyjskie Terytorium Oceanu Indyjskiego">
																							Brytyjskie Terytorium Oceanu Indyjskiego </option>
																						<option value="Brunei Darussalam" data-calc-value="Brunei Darussalam">
																							Brunei Darussalam </option>
																						<option value="Bulgaria" data-calc-value="Bulgaria">
																							Bułgaria </option>
																						<option value="Burkina Faso" data-calc-value="Burkina Faso">
																							Burkina Faso </option>
																						<option value="Burundi" data-calc-value="Burundi">
																							Burundi </option>
																						<option value="Cambodia" data-calc-value="Cambodia">
																							Kambodża </option>
																						<option value="Cameroon" data-calc-value="Cameroon">
																							Kamerun </option>
																						<option value="Canada" data-calc-value="Canada">
																							Kanada </option>
																						<option value="Cape Verde" data-calc-value="Cape Verde">
																							Republika Zielonego Przylądka </option>
																						<option value="Kajmany" data-calc-value="Kajmany">
																							Kajmany </option>
																						<wartość opcji=„Republika Środkowoafrykańska”
																							data-calc-value="Republika Środkowoafrykańska">
																							Republika Środkowoafrykańska </option>
																							<option value="Chad" data-calc-value="Chad">
																								Czad </option>
																							<option value="Chile" data-calc-value="Chile">
																								Chile </option>
																							<option value="China" data-calc-value="China">
																								Chiny </option>
																							<option value="Wyspa Bożego Narodzenia"
																								data-calc-value="Wyspa Bożego Narodzenia">
																								Wyspa Bożego Narodzenia </option>
																							<option value="Wyspy Kokosowe (Keelinga)"
																								data-calc-value="Wyspy Kokosowe (Keelinga)">
																								Wyspy Kokosowe (Keelinga) </option>
																							<option value="Colombia" data-calc-value="Colombia">
																								Kolumbia </option>
																							<option value="Comoros" data-calc-value="Comoros">
																								Komory </option>
																							<option value="Congo" data-calc-value="Congo">
																								Kongo </option>
																							<option value="Kongo, Demokratyczna Republika"
																								data-calc-value="Kongo, Demokratyczna Republika">
																								Kongo, Demokratyczna Republika </option>
																							<option value="Wyspy Cooka" data-calc-value="Wyspy Cooka">
																								Wyspy Cooka </option>
																							<option value="Costa Rica" data-calc-value="Costa Rica">
																								Kostaryka </option>
																							<option value="Côte d & # 039; Ivoire"
																								data-calc-value="Côte d & # 039; Ivoire">
																								Wybrzeże Kości Słoniowej </option>
																							<option value="Croatia" data-calc-value="Croatia">
																								Chorwacja </option>
																							<option value="Cuba" data-calc-value="Cuba">
																								Kuba </option>
																							<option value="Curaçao" data-calc-value="Curaçao">
																								Curaçao </option>
																							<option value="Cyprus" data-calc-value="Cyprus">
																								Cypr </option>
																							<option value="Czech Republic" data-calc-value="Czech Republic">
																								Republika Czeska </option>
																							<option value="Dania" data-calc-value="Dania">
																								Dania </option>
																							<option value="Djibouti" data-calc-value="Djibouti">
																								Dżibuti </option>
																							<option value="Dominica" data-calc-value="Dominica">
																								Dominika </option>
																							<option value="Dominikana" data-calc-value="Dominikana">
																								Republika Dominikany </option>
																							<option value="Ecuador" data-calc-value="Ecuador">
																								Ekwador </option>
																							<option value="Egypt" data-calc-value="Egypt">
																								Egipt </option>
																							<option value="El Salvador" data-calc-value="El Salvador">
																								Salwador </option>
																							<option value="Gwinea Równikowa" data-calc-value="Gwinea Równikowa">
																								Gwinea Równikowa </option>
																							<option value="Erytrea" data-calc-value="Erytrea">
																								Erytrea </option>
																							<option value="Estonia" data-calc-value="Estonia">
																								Estonia </option>
																							<option value="Ethiopia" data-calc-value="Ethiopia">
																								Etiopia </option>
																							<option value="Falklandy (Malwiny)" data-calc-value="Falklandy (Malwiny)">
																								Falklandy (Malwiny) </option>
																							<option value="Wyspy Owcze" data-calc-value="Wyspy Owcze">
																								Wyspy Owcze </option>
																							<option value="Fiji" data-calc-value="Fiji">
																								Fidżi </option>
																							<option value="Finland" data-calc-value="Finland">
																								Finlandia </option>
																							<option value="France" data-calc-value="France">
																								Francja </option>
																							<option value="French Guiana" data-calc-value="French Guiana">
																								Gujana Francuska </option>
																							<option value="French Polinesia" data-calc-value="French Polinesia">
																								Polinezja Francuska </option>
																							<option value="Francuskie Terytoria Południowe"
																								data-calc-value="Francuskie Terytoria Południowe">
																								Francuskie Terytoria Południowe </option>
																							<option value="Gabon" data-calc-value="Gabon">
																								Gabon </option>
																							<option value="Gambia" data-calc-value="Gambia">
																								Gambia </option>
																							<option value="Georgia" data-calc-value="Georgia">
																								Gruzja </option>
																							<option value="Germany" data-calc-value="Germany">
																								Niemcy </option>
																							<option value="Ghana" data-calc-value="Ghana">
																								Ghana </option>
																							<option value="Gibraltar" data-calc-value="Gibraltar">
																								Gibraltar </option>
																							<option value="Greece" data-calc-value="Greece">
																								Grecja </option>
																							<option value="Greenland" data-calc-value="Greenland">
																								Grenlandia </option>
																							<option value="Grenada" data-calc-value="Grenada">
																								Grenada </option>
																							<option value="Guadeloupe" data-calc-value="Guadeloupe">
																								Gwadelupa </option>
																							<option value="Guam" data-calc-value="Guam">
																								Guam </option>
																							<option value="Guatemala" data-calc-value="Guatemala">
																								Gwatemala </option>
																							<option value="Guernsey" data-calc-value="Guernsey">
																								Guernsey </option>
																							<option value="Guinea" data-calc-value="Guinea">
																								Gwinea </option>
																							<option value="Guinea-Bissau" data-calc-value="Guinea-Bissau">
																								Gwinea Bissau </option>
																							<option value="Guyana" data-calc-value="Guyana">
																								Gujana </option>
																							<option value="Haiti" data-calc-value="Haiti">
																								Haiti </option>
																							<option value="Wyspy Heard i McDonalda"
																								data-calc-value="Wyspy Heard i Wyspy McDonalda">
																								Wyspy Heard i Wyspy McDonalda </option>
																							<option value="Stolica Apostolska (Państwo Watykańskie)"
																								data-calc-value="Stolica Apostolska (Państwo Watykańskie)">
																								Stolica Apostolska (Państwo Watykańskie) </option>
																							<option value="Honduras" data-calc-value="Honduras">
																								Honduras </option>
																							<option value="Hong Kong" data-calc-value="Hong Kong">
																								Hongkong </option>
																							<option value="Hungary" data-calc-value="Hungary">
																								Węgry </option>
																							<option value="Iceland" data-calc-value="Iceland">
																								Islandia </option>
																							<option value="India" data-calc-value="India">
																								Indie </option>
																							<option value="Indonesia" data-calc-value="Indonesia">
																								Indonezja </option>
																							<option value="Iran, Islamic Republic of"
																								data-calc-value="Iran, Islamska Republika">
																								Iran, Islamska Republika </option>
																							<option value="Irak" data-calc-value="Irak">
																								Irak </option>
																							<option value="Ireland" data-calc-value="Ireland">
																								Irlandia </option>
																							<option value="Isle of Man" data-calc-value="Isle of Man">
																								Wyspa Man </option>
																							<option value="Israel" data-calc-value="Israel">
																								Izrael </option>
																							<option value="Italy" data-calc-value="Italy">
																								Włochy </option>
																							<option value="Jamaica" data-calc-value="Jamaica">
																								Jamajka </option>
																							<option value="Japan" data-calc-value="Japan">
																								Japonia </option>
																							<option value="Jersey" data-calc-value="Jersey">
																								Jersey </option>
																							<option value="Jordan" data-calc-value="Jordan">
																								Jordania </option>
																							<option value="Kazakhstan" data-calc-value="Kazakhstan">
																								Kazachstan </option>
																							<option value="Kenia" data-calc-value="Kenia">
																								Kenia </option>
																							<option value="Kiribati" data-calc-value="Kiribati">
																								Kiribati </option>
																							<option value="Koreańska Republika Ludowo-Demokratyczna"
																								data-calc-value="Koreańska Republika Ludowo-Demokratyczna">
																								Koreańska Republika Ludowo-Demokratyczna </option>
																							<option value="Korea, Republic of" data-calc-value="Korea, Republic of">
																								Republika Korei </option>
																							<option value="Kuwejt" data-calc-value="Kuwejt">
																								Kuwejt </option>
																							<option value="Kyrgyzstan" data-calc-value="Kyrgyzstan">
																								Kirgistan </option>
																							<option value=„Laotańska Republika Ludowo-Demokratyczna”
																								data-calc-value="Laotańska Republika Ludowo-Demokratyczna">
																								Laotańska Republika Ludowo-Demokratyczna </option>
																							<option value="Łotwa" data-calc-value="Łotwa">
																								Łotwa </option>
																							<option value="Liban" data-calc-value="Liban">
																								Liban </option>
																							<option value="Lesotho" data-calc-value="Lesotho">
																								Lesotho </option>
																							<option value="Liberia" data-calc-value="Liberia">
																								Liberia </option>
																							<option value="Libya" data-calc-value="Libya">
																								Libia </option>
																							<option value="Liechtenstein" data-calc-value="Liechtenstein">
																								Liechtenstein </option>
																							<option value="Lithuania" data-calc-value="Lithuania">
																								Litwa </option>
																							<option value="Luxembourg" data-calc-value="Luxembourg">
																								Luksemburg </option>
																							<option value="Macao" data-calc-value="Macao">
																								Makao </option>
																							<option value="Macedonia, była jugosłowiańska republika"
																								data-calc-value="Macedonia, Była Jugosłowiańska Republika">
																								Macedonia, była jugosłowiańska republika </option>
																							<option value="Madagascar" data-calc-value="Madagascar">
																								Madagaskar </option>
																							<option value="Malawi" data-calc-value="Malawi">
																								Malawi </option>
																							<option value="Malaysia" data-calc-value="Malaysia">
																								Malezja </option>
																							<option value="Maledives" data-calc-value="Maledives">
																								Malediwy </option>
																							<option value="Mali" data-calc-value="Mali">
																								Mali </option>
																							<option value="Malta" data-calc-value="Malta">
																								Malta </option>
																							<option value="Marshall Islands" data-calc-value="Marshall Islands">
																								Wyspy Marshalla </option>
																							<option value="Martinique" data-calc-value="Martinique">
																								Martynika </option>
																							<option value="Mauretania" data-calc-value="Mauretania">
																								Mauretania </option>
																							<option value="Mauritius" data-calc-value="Mauritius">
																								Mauritius </option>
																							<option value="Mayotte" data-calc-value="Mayotte">
																								Majotta </option>
																							<option value="Mexico" data-calc-value="Mexico">
																								Meksyk </option>
																							<option value="Mikronezja, Sfederowane Stany"
																								data-calc-value="Mikronezja, Sfederowane Stany">
																								Mikronezja, Sfederowane Stany </option>
																							<option value="Mołdawia, Republika" data-calc-value="Mołdawia, Republika">
																								Mołdawia, Republika </option>
																							<option value="Monaco" data-calc-value="Monaco">
																								Monako </option>
																							<option value="Mongolia" data-calc-value="Mongolia">
																								Mongolia </option>
																							<option value="Montenegro" data-calc-value="Montenegro">
																								Czarnogóra </option>
																							<option value="Montserrat" data-calc-value="Montserrat">
																								Montserrat </option>
																							<option value="Morocco" data-calc-value="Morocco">
																								Maroko </option>
																							<option value="Mozambik" data-calc-value="Mozambik">
																								Mozambik </option>
																							<option value="Myanmar" data-calc-value="Myanmar">
																								Birma </option>
																							<option value="Namibia" data-calc-value="Namibia">
																								Namibia </option>
																							<option value="Nauru" data-calc-value="Nauru">
																								Nauru </option>
																							<option value="Nepal" data-calc-value="Nepal">
																								Nepal </option>
																							<option value="Netherlands" data-calc-value="Netherlands">
																								Holandia </option>
																							<option value="Nowa Kaledonia" data-calc-value="Nowa Kaledonia">
																								Nowa Kaledonia </option>
																							<option value="New Zealand" data-calc-value="New Zealand">
																								Nowa Zelandia </option>
																							<option value="Nikaragua" data-calc-value="Nikaragua">
																								Nikaragua </option>
																							<option value="Niger" data-calc-value="Niger">
																								Niger </option>
																							<option value="Nigeria" data-calc-value="Nigeria">
																								Nigeria </option>
																							<option value="Niue" data-calc-value="Niue">
																								Niue </option>
																							<option value="Norfolk Island" data-calc-value="Norfolk Island">
																								Wyspa Norfolk </option>
																							<option value="Mariany Północne" data-calc-value="Mariany Północne">
																								Mariany Północne </option>
																							<option value="Norway" data-calc-value="Norway">
																								Norwegia </option>
																							<option value="Oman" data-calc-value="Oman">
																								Oman </option>
																							<option value="Pakistan" data-calc-value="Pakistan">
																								Pakistan </option>
																							<option value="Palau" data-calc-value="Palau">
																								Palau </option>
																							<option value="Okupowane Terytorium Palestyny"
																								data-calc-value="Okupowane Terytorium Palestyny">
																								Okupowane Terytorium Palestyny ​​</option>
																							<option value="Panama" data-calc-value="Panama">
																								Panama </option>
																							<option value="Papua-Nowa Gwinea" data-calc-value="Papua-Nowa Gwinea">
																								Papua Nowa Gwinea </option>
																							<option value="Paragwaj" data-calc-value="Paragwaj">
																								Paragwaj </option>
																							<option value="Peru" data-calc-value="Peru">
																								Peru </option>
																							<option value="Filipiny" data-calc-value="Filipiny">
																								Filipiny </option>
																							<option value="Pitcairn" data-calc-value="Pitcairn">
																								Pitcairn </option>
																							<option value="Poland" data-calc-value="Poland">
																								Polska </option>
																							<option value="Portugal" data-calc-value="Portugal">
																								Portugalia </option>
																							<option value="Puerto Rico" data-calc-value="Puerto Rico">
																								Portoryko </option>
																							<option value="Qatar" data-calc-value="Qatar">
																								Katar </option>
																							<option value="Réunion" data-calc-value="Reunion">
																								Reunion </option>
																							<option value="Romania" data-calc-value="Romania">
																								Rumunia </option>
																							<option value="Russian Federation" data-calc-value="Russian Federation">
																								Federacja Rosyjska </option>
																							<option value="Rwanda" data-calc-value="Rwanda">
																								Rwanda </option>
																							<option value="Saint Barthélemy" data-calc-value="Saint Barthélemy">
																								Saint Barthélemy </option>
																							<wartość opcji=„Święta Helena, Wniebowstąpienie i Tristan da Cunha”
																								data-calc-value="Święta Helena, Wniebowstąpienie i Tristan da Cunha">
																								Święta Helena, Wniebowstąpienie i Tristan da Cunha </option>
																								<option value="Saint Kitts i Nevis"
																									data-calc-value="Saint Kitts i Nevis">
																									Saint Kitts i Nevis </option>
																								<option value="Saint Lucia" data-calc-value="Saint Lucia">
																									Saint Lucia </option>
																								<wartość opcji=„Saint Martin (część francuska)”
																									data-calc-value="Saint Martin (część francuska)">
																									Saint Martin (część francuska) </option>
																									<wartość opcji=„Saint-Pierre i Miquelon”
																										data-calc-value="Saint-Pierre i Miquelon">
																										Saint-Pierre i Miquelon </option>
																										<option value=„Saint Vincent i Grenadyny”
																											data-calc-value="Saint Vincent i Grenadyny">
																											Saint Vincent i Grenadyny </option>
																										<option value="Samoa" data-calc-value="Samoa">
																											Samoa </option>
																										<option value="San Marino" data-calc-value="San Marino">
																											San Marino </option>
																										<option value="Wyspy Świętego Tomasza i Książęca"
																											data-calc-value="Wyspy Świętego Tomasza i Książęca">
																											Wyspy Świętego Tomasza i Książęca </option>
																										<option value="Saudi Arabia" data-calc-value="Saudi Arabia">
																											Arabia Saudyjska </option>
																										<option value="Senegal" data-calc-value="Senegal">
																											Senegal </option>
																										<option value="Serbia" data-calc-value="Serbia">
																											Serbia </option>
																										<option value="Seychelles" data-calc-value="Seychelles">
																											Seszele </option>
																										<option value="Sierra Leone" data-calc-value="Sierra Leone">
																											Sierra Leone </option>
																										<option value="Singapore" data-calc-value="Singapore">
																											Singapur </option>
																										<option value="Sint Maarten (część holenderska)"
																											data-calc-value="Sint Maarten (część holenderska)">
																											Sint Maarten (część holenderska) </option>
																										<option value="Slovakia" data-calc-value="Slovakia">
																											Słowacja </option>
																										<option value="Slovenia" data-calc-value="Slovenia">
																											Słowenia </option>
																										<option value="Salomon Islands" data-calc-value="Salomon Islands">
																											Wyspy Salomona </option>
																										<option value="Somalia" data-calc-value="Somalia">
																											Somalia </option>
																										<option value="South Africa" ​​data-calc-value="South Africa">
																											Republika Południowej Afryki </option>
																										<option value="Georgia Południowa i Sandwich Południowy"
																											data-calc-value="Georgia Południowa i Sandwich Południowy">
																											Georgia Południowa i Sandwich Południowy </option>
																										<option value="South Sudan" data-calc-value="South Sudan">
																											Sudan Południowy </option>
																										<option value="Spain" data-calc-value="Spain">
																											Hiszpania </option>
																										<option value="Sri Lanka" data-calc-value="Sri Lanka">
																											Sri Lanka </option>
																										<option value="Sudan" data-calc-value="Sudan">
																											Sudan </option>
																										<option value="Surinam" data-calc-value="Surinam">
																											Surinam </option>
																										<wartość opcji=„Svalbard i Jan Mayen”
																											data-calc-value="Svalbard i Jan Mayen">
																											Svalbard i Jan Mayen </option>
																											<option value="Swaziland" data-calc-value="Swaziland">
																												Suazi </option>
																											<option value="Sweden" data-calc-value="Sweden">
																												Szwecja </option>
																											<option value="Switzerland" data-calc-value="Switzerland">
																												Szwajcaria </option>
																											<option value="Syryjska Republika Arabska"
																												data-calc-value="Syryjska Republika Arabska">
																												Syryjska Republika Arabska </option>
																											<option value="Tajwan, prowincja Chin"
																												data-calc-value="Tajwan, prowincja Chin">
																												Tajwan, Prowincja Chińska </option>
																											<option value="Tadżykistan" data-calc-value="Tadżykistan">
																												Tadżykistan </option>
																											<wartość opcji=„Tanzania, Zjednoczona Republika”
																												data-calc-value="Tanzania, Zjednoczona Republika">
																												Tanzania, Zjednoczona Republika </option>
																												<option value="Thailand" data-calc-value="Thailand">
																													Tajlandia </option>
																												<option value="Timor-Leste" data-calc-value="Timor-Leste">
																													Timor Wschodni </option>
																												<option value="Togo" data-calc-value="Togo">
																													Togo </option>
																												<option value="Tokelau" data-calc-value="Tokelau">
																													Tokelau </option>
																												<option value="Tonga" data-calc-value="Tonga">
																													Tonga </option>
																												<option value="Trinidad and Tobago"
																													data-calc-value="Trinidad and Tobago">
																													Trynidad i Tobago </option>
																												<option value="Tunezja" data-calc-value="Tunezja">
																													Tunezja </option>
																												<option value="Turkey" data-calc-value="Turkey">
																													Turcja </option>
																												<option value="Turkmenistan" data-calc-value="Turkmenistan">
																													Turkmenistan </option>
																												<option value="Turks i Caicos"
																													data-calc-value="Wyspy Turks i Caicos">
																													Wyspy Turks i Caicos </option>
																												<option value="Tuvalu" data-calc-value="Tuvalu">
																													Tuvalu </option>
																												<option value="Uganda" data-calc-value="Uganda">
																													Uganda </option>
																												<option value="Ukraina" data-calc-value="Ukraina">
																													Ukraina </option>
																												<option value="Zjednoczone Emiraty Arabskie"
																													data-calc-value="Zjednoczone Emiraty Arabskie">
																													Zjednoczone Emiraty Arabskie </option>
																												<option value="United Kingdom" data-calc-value="United Kingdom">
																													Wielka Brytania </option>
																												<option value="United States" data-calc-value="United States">
																													Stany Zjednoczone </option>
																												<option value="Dalekie Wyspy Mniejsze Stanów Zjednoczonych"
																													data-calc-value="Dalekie Wyspy Mniejsze Stanów Zjednoczonych">
																													Dalekie Wyspy Mniejsze Stanów Zjednoczonych </option>
																												<option value="Uruguay" data-calc-value="Uruguay">
																													Urugwaj </option>
																												<option value="Uzbekistan" data-calc-value="Uzbekistan">
																													Uzbekistan </option>
																												<option value="Vanuatu" data-calc-value="Vanuatu">
																													Vanuatu </option>
																												<option value="Wenezuela, Boliwariańska Republika"
																													data-calc-value="Wenezuela, Boliwariańska Republika">
																													Wenezuela, Boliwariańska Republika </option>
																												<option value="Wietnam" data-calc-value="Wietnam">
																													Wietnam </option>
																												<option value="Brytyjskie Wyspy Dziewicze"
																													data-calc-value="Brytyjskie Wyspy Dziewicze">
																													Brytyjskie Wyspy Dziewicze </option>
																												<option value="Wyspy Dziewicze Stanów Zjednoczonych"
																													data-calc-value="Wyspy Dziewicze Stanów Zjednoczonych">
																													Wyspy Dziewicze Stanów Zjednoczonych </option>
																												<option value="Wallis i Futuna"
																													data-calc-value="Wallis i Futuna">
																													Wallis i Futuna </option>
																												<option value="Western Sahara" data-calc-value="Western Sahara">
																													Sahara Zachodnia </option>
																												<option value="Jemen" data-calc-value="Jemen">
																													Jemen </option>
																												<option value="Zambia" data-calc-value="Zambia">
																													Zambia </option>
																												<option value="Zimbabwe" data-calc-value="Zimbabwe">
																													Zimbabwe </option>
																				</select>
																			</div>
																			<span class="help-block caldera_ajax_error_block filled" id="parsley-id-11"
																				aria-live="polite" hidden="true"><span class="parsley-required"
																					style="color: #a94442">Ta wartość jest
																					wymagana.</span></span>
																		</div>
																		<!-- SmartPhone Model -->
																		<div data-field-wrapper="fld_8498340" class="form-group" id="fld_8498340_1-wrap">
																			<label id="fld_8498340Label" for="fld_8498340_1"
																				class="control-label screen-reader-text sr-only">SmartPhone Model</label>
																			<div class="">
																				<input placeholder="SmartPhone Model" type="text" class="form-control"
																					id="fld_8498340_1" name="phoneModel" value="">
																			</div>
																		</div>
																		<!-- Smart App -->
																		<div data-field-wrapper="fld_1623657" class="form-group" id="fld_1623657_1-wrap">
																			<label id="fld_1623657Label" for="fld_1623657_1"
																				class="control-label screen-reader-text sr-only"> MIR Smart One App
																				wersja </label>
																			<div class="">
																				<input placeholder="MIR Smart One App	wersja *" type="text"
																					class=" form-control" id="fld_1623657_1" name="smartApp" value="">
																			</div>
																			<span class="help-block caldera_ajax_error_block filled" id="parsley-id-13"
																				aria-live="polite" hidden="true"><span class="parsley-required"
																					style="color: #a94442">Ta wartość jest
																					wymagana.</span></span>
																		</div>

																	</div>
																	<div class="col-sm-1 "></div>
																	<div class="col-sm-5  last_col">
																		<!-- Phone number -->
																		<div data-field-wrapper="fld_6827714" class="form-group" id="fld_6827714_1-wrap">
																			<label id="fld_6827714Label" for="fld_6827714_1"
																				class="control-label-screen-reader-text sr-only"> Telefon </label>
																			<div class="">
																				<input placeholder="Telefon" data-inputmask="'mask': '+99 99 999 9999'"
																					type="phone" class=" form-control" id="fld_6827714_1" name="phoneModel"
																					value="">
																			</div>
																		</div>
																		<!-- Discription -->
																		<div data-field-wrapper="fld_4833399" class="form-group" id="fld_4833399_1-wrap">
																			<label id="fld_4833399Label" for="fld_4833399_1"
																				class="control-label-screen-reader-text sr-only"> Opis </label>
																			<div class="">
																				<textarea name="description" value="" class="form-control" id="fld_4833399_1"
																					rows="1" placeholder="Opis *"></textarea>
																			</div>
																			<span class="help-block caldera_ajax_error_block filled" id="parsley-id-15"
																				aria-live="polite" hidden="true"><span class="parsley-required"
																					style="color: #a94442">Ta wartość jest
																					wymagana.</span></span>
																		</div>
																		<!-- submit -->
																		<div data-field-wrapper="fld_1889239" class="form-group" id="fld_1889239_1-wrap">
																			<label class="control-label">&nbsp;</label>
																			<div class="">
																				<input class="btn btn-default" type="submit" name="fld_1889239"
																					id="fld_1889239_1" value="WYSŁAĆ" data-field="fld_1889239">
																				<!-- <div class="btn btn-default send-form" name="fld_1889239" id="fld_1889239_1">
																					"WYSŁAĆ"</div>
																			</div> -->
																		</div>

																		<input class="button_trigger_1" type="hidden" name="fld_1889239"
																			id="fld_1889239_1_btn" value="" data-field="fld_1889239">
																	</div>
																</div>
																<div id="CF5bf49ccb9928d_1-row-2" class="row  last_row">
																	<div class="col-sm-12  single">
																		<div data-field-wrapper="fld_5416591" class="form-group" id="fld_5416591_1-wrap">
																			<div class="">

																				<div class="checkbox-inline caldera-forms-consent-field">
																					<input type="checkbox" id="fld_5416591_1"
																						class="fld_5416591_1 option-required" name="fld_5416591">
																					<label for="fld_5416591_1" class="caldera-form-gdpr-field-label"
																						style="display: inline; margin-left: 0.5rem;">
																						<p class="caldera-formularzy-zgoda-pola-zgody">
																							UJAWNIENIE O przetwarzaniu danych osobowych zgodnie z artykułem 13 UE
																							Rozporządzenie 679/2016.
																							Wyrażam zgodę na przetwarzanie moich danych osobowych w celach
																							marketingowych
																							celów i przekazów handlowych za pośrednictwem różnych mediów,
																							w tym elektroniczne i telematyczne (e-mail, sms) i tradycyjne
																							(poczta papierowa i telefon) kanały. </p>
																						<a href="" target="_ blank" title="Strona z polityką prywatności"
																							class="caldera-forms-expression-field-linked_text">
																							Polityka prywatności </a>
																					</label>
																					<span style="color:#ff0000;">*</span>
																					<span class="help-block caldera_ajax_error_block filled" id="parsley-id-17"
																						aria-live="polite" hidden="true">
																						<span class="parsley-required" style="color: #a94442">Ta wartość jest
																							wymagana.</span></span>
																				</div>

																			</div>
																		</div>
																	</div>
																</div>
															</form>
															<!-- END of FORM															 -->

														</div>
													</div>
												</div>
												<div
													class="elementor-element elementor-element-4d38 elementor-widget elementor-widget-text-editor"
													data-id="4d38" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> Pola oznaczone * są wymagane </p>
														</div>
													</div>
												</div>
												<div class="elementor-element elementor-element-54ba elementor-widget elementor-widget-divider"
													data-id="54ba" data-element_type="widget" data-widget_type="divider.default">
													<div class="elementor-widget-container">
														<div class="elementor-divider">
															<span class="elementor-divider-separator">
															</span>
														</div>
													</div>
												</div>
												<section
													class="elementor-element elementor-element-47f4 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="47f4" data-element_type="section">
													<div class="elementor-container elementor-column-gap-default">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-7ee6 elementor-column elementor-col-100 elementor-inner-column"
																data-id="7ee6" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-2cc3 elementor-position-right elementor-vertical-align-middle elementor-view-stacked elementor-shape-circle elementor-widget elementor-widget-icon-box"
																			data-id="2cc3" data-element_type="widget" data-widget_type="icon-box.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-icon-box-wrapper">
																					<div class="elementor-icon-box-icon">
																						<a class="elementor-icon elementor-animation-" href="contact/index.htm"
																							target="_blank">
																							<i aria-hidden="true" class="fas fa-download"></i> </a>
																					</div>
																					<div class="elementor-icon-box-content">
																						<h4 class="elementor-icon-box-title">
																							<a href="contact/index.htm" target="_blank">przeczytać
																								Publikacje naukowe </a>
																						</h4>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
												<section
													class="elementor-element elementor-element-758 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
													data-id="758" data-element_type="section">
													<div class="elementor-container elementor-column-gap-default">
														<div class="elementor-row">
															<div
																class="elementor-element elementor-element-4abc elementor-column elementor-col-100 elementor-inner-column"
																data-id="4abc" data-element_type="column">
																<div class="elementor-column-wrap  elementor-element-populated">
																	<div class="elementor-widget-wrap">
																		<div
																			class="elementor-element elementor-element-3d6 elementor-shape-circle elementor-widget elementor-widget-social-icons"
																			data-id="3d6" data-element_type="widget" data-widget_type="social-icons.default">
																			<div class="elementor-widget-container">
																				<div class="elementor-social-icons-wrapper">
																					<a class="elementor-icon elementor-social-icon elementor-social-icon-facebook elementor-repeater-item-38d51c2"
																						href="https://www.facebook.com/Carebits" target="_blank">
																						<span class="elementor-screen-only"> Facebook </span>
																						<i class="fa fa-facebook"> </i>
																					</a>
																					<a class="elementor-icon elementor-social-icon elementor-social-icon-linkedin elementor-repeater-item-20b46d2"
																						href="https://www.instagram.com/carebits_ktg/" target="_blank">
																						<span class="elementor-screen-only"> Linkedin </span>
																						<i class="fa fa-instagram"> </i>
																					</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-30df elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="30df" data-element_type="section"
							data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-6c37 elementor-column elementor-col-25 elementor-top-column"
										data-id="6c37" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-5737 elementor-widget elementor-widget-heading"
													data-id="5737" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h3 class="elementor-heading-title elementor-size-medium">MIR</h3>
													</div>
												</div>
												<div class="elementor-element elementor-element-61f5 elementor-widget elementor-widget-counter"
													data-id="61f5" data-element_type="widget" data-widget_type="counter.default">
													<div class="elementor-widget-container">
														<div class="elementor-counter">
															<div class="elementor-counter-number-wrapper">
																<span class="elementor-counter-number-prefix"></span>
																<span class="elementor-counter-number" data-duration="2000" data-to-value="28"
																	data-from-value="0" data-delimiter=",">0</span>
																<span class="elementor-counter-number-suffix"></span>
															</div>
															<div class="elementor-counter-title">LAT</div>
														</div>
													</div>
												</div>
												<div class="elementor-element elementor-element-220e elementor-widget elementor-widget-heading"
													data-id="220e" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h4 class="elementor-heading-title elementor-size-default">DOŚWIADCZENIE I WIEDZA W
															SPIROMETRYSETTORE </h4>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-1c74 elementor-column elementor-col-25 elementor-top-column"
										data-id="1c74" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-2c10 elementor-widget elementor-widget-heading"
													data-id="2c10" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h3 class="elementor-heading-title elementor-size-large">NA ŚWIECIE </h3>
													</div>
												</div>
												<div class="elementor-element elementor-element-1875 elementor-widget elementor-widget-counter"
													data-id="1875" data-element_type="widget" data-widget_type="counter.default">
													<div class="elementor-widget-container">
														<div class="elementor-counter">
															<div class="elementor-counter-number-wrapper">
																<span class="elementor-counter-number-prefix"></span>
																<span class="elementor-counter-number" data-duration="2000" data-to-value="100"
																	data-from-value="0" data-delimiter=",">0</span>
																<span class="elementor-counter-number-suffix"></span>
															</div>
															<div class="elementor-counter-title">KRAJE </div>
														</div>
													</div>
												</div>
												<div class="elementor-element elementor-element-7aa3 elementor-widget elementor-widget-heading"
													data-id="7aa3" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h4 class="elementor-heading-title elementor-size-default">UŻYJ MIR <br>
															SPIROMETER </h4>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-197f elementor-column elementor-col-25 elementor-top-column"
										data-id="197f" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-62a5 elementor-widget elementor-widget-heading"
													data-id="62a5" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h3 class="elementor-heading-title elementor-size-large">PATENTY </h3>
													</div>
												</div>
												<div class="elementor-element elementor-element-3f16 elementor-widget elementor-widget-counter"
													data-id="3f16" data-element_type="widget" data-widget_type="counter.default">
													<div class="elementor-widget-container">
														<div class="elementor-counter">
															<div class="elementor-counter-number-wrapper">
																<span class="elementor-counter-number-prefix"></span>
																<span class="elementor-counter-number" data-duration="2000" data-to-value="8"
																	data-from-value="0" data-delimiter=",">0</span>
																<span class="elementor-counter-number-suffix"></span>
															</div>
															<div class="elementor-counter-title">PRODUCT </div>
														</div>
													</div>
												</div>
												<div class="elementor-element elementor-element-46e6 elementor-widget elementor-widget-heading"
													data-id="46e6" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h4 class="elementor-heading-title elementor-size-default">OBJĘTE
															EKSKLUZYWNE <br> MIĘDZYNARODOWE PATENTY </h4>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-5a8d elementor-column elementor-col-25 elementor-top-column"
										data-id="5a8d" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-5f43 elementor-widget elementor-widget-heading"
													data-id="5f43" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h3 class="elementor-heading-title elementor-size-large">BADANIA </h3>
													</div>
												</div>
												<div class="elementor-element elementor-element-223b elementor-widget elementor-widget-counter"
													data-id="223b" data-element_type="widget" data-widget_type="counter.default">
													<div class="elementor-widget-container">
														<div class="elementor-counter">
															<div class="elementor-counter-number-wrapper">
																<span class="elementor-counter-number-prefix"></span>
																<span class="elementor-counter-number" data-duration="2000" data-to-value="16"
																	data-from-value="0" data-delimiter=",">0</span>
																<span class="elementor-counter-number-suffix">%</span>
															</div>
															<div class="elementor-counter-title">ROCZNYCH OBROTÓW </div>
														</div>
													</div>
												</div>
												<div class="elementor-element elementor-element-75b7 elementor-widget elementor-widget-heading"
													data-id="75b7" data-element_type="widget" data-widget_type="heading.default">
													<div class="elementor-widget-container">
														<h4 class="elementor-heading-title elementor-size-default">JEST ZAINWESTOWANA <br>
															W R&D </h4>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section
							class="elementor-element elementor-element-18 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
							data-id="18" data-element_type="section"
							data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-container elementor-column-gap-default">
								<div class="elementor-row">
									<div
										class="elementor-element elementor-element-7fca elementor-column elementor-col-33 elementor-top-column"
										data-id="7fca" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-7759 elementor-widget elementor-widget-text-editor"
													data-id="7759" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> <span style="font-size: 80%;"> MIR POLAND </span> <br> <span style="font-size: 80%;">
																	Med Systems sp. z o.o., <br>
																	ul. Nowy Świat 57/59, <br>
																	lok. 12. 00-042 Warszawa </span> <br>
																<span style="font-size: 80%;"> zarzad@med-systems.pl <br>
																</span>
																<span style="font-size: 80%;"> +48 888 065 534; +48 514 951 077 </span>
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-1907 elementor-column elementor-col-33 elementor-top-column"
										data-id="1907" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div class="elementor-element elementor-element-6a48 elementor-widget elementor-widget-image"
													data-id="6a48" data-element_type="widget" data-widget_type="image.default">
													<div class="elementor-widget-container">
														<div class="elementor-image">
															<img width="240" height="81" src="wp-content/uploads/2018/09/LogoMir.png"
																class="attachment-full size-full" alt="SMART ONE | MIR Spirometro tascabile personale"
																loading="lazy">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div
										class="elementor-element elementor-element-4f45 elementor-column elementor-col-33 elementor-top-column"
										data-id="4f45" data-element_type="column">
										<div class="elementor-column-wrap  elementor-element-populated">
											<div class="elementor-widget-wrap">
												<div
													class="elementor-element elementor-element-1513 elementor-widget elementor-widget-text-editor"
													data-id="1513" data-element_type="widget" data-widget_type="text-editor.default">
													<div class="elementor-widget-container">
														<div class="elementor-text-editor elementor-clearfix">
															<p> <span style="font-size: 80%;"> MIR POLAND </span> <br> <span style="font-size: 80%;">
																	Med Systems sp. z o.o., <br>
																	ul. Nowy Świat 57/59, <br>
																	lok. 12. 00-042 Warszawa </span> <br>
																<span style="font-size: 80%;"> zarzad@med-systems.pl <br>
																</span>
																<span style="font-size: 80%;"> +48 888 065 534; +48 514 951 077 </span>
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>

		</div><!-- #main -->

	</div><!-- #wrapper -->

	<!-- Mobile Sidebar -->
	<div id="main-menu" class="mobile-sidebar no-scrollbar mfp-hide">
		<div class="sidebar-menu no-scrollbar ">
			<ul class="nav nav-sidebar  nav-vertical nav-uppercase">
				<li id="menu-item-1603" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1603"><a
						href="index-2.htm?page_id=1587&amp;lang=en#SPIROMETER" class="nav-top-link">SPIROMETER</a></li>
				<li id="menu-item-1608" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1608"><a
						href="index-2.htm?page_id=1587&amp;lang=en#HEALTH" class="nav-top-link">YOUR HEALTH</a></li>
				<li id="menu-item-1609" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1609"><a
						href="index-2.htm?page_id=1587&amp;lang=en#STRENGHTS" class="nav-top-link">STRENGTHS</a></li>
				<li id="menu-item-1610" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1610"><a
						href="index-2.htm?page_id=1587&amp;lang=en#app" class="nav-top-link">THE APP</a></li>
				<li id="menu-item-1611" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1611"><a
						href="index-2.htm?page_id=1587&amp;lang=en#COMPANY" class="nav-top-link">THE COMPANY</a></li>
				<li id="menu-item-1612" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1612"><a
						href="index-2.htm?page_id=1587&amp;lang=en#AWARDS" class="nav-top-link">AWARDS</a></li>
				<li id="menu-item-1613" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1613"><a
						href="index-2.htm?page_id=1587&amp;lang=en#ASSISTANCE" class="nav-top-link">ASSISTANCE</a></li>
				<li id="menu-item-2563" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2563"><a
						href="index-7.htm?page_id=2491&amp;lang=en" class="nav-top-link">BLOG</a></li>
				<li id="menu-item-1604"
					class="pll-parent-menu-item menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1604">
					<a href="#pll_switcher" class="nav-top-link"><img
							src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAflJREFUeNpinDRzn5qN3uFDt16+YWBg+Pv339+KGN0rbVP+//2rW5tf0Hfy/2+mr99+yKpyOl3Ydt8njEWIn8f9zj639NC7j78eP//8739GVUUhNUNuhl8//ysKeZrJ/v7z10Zb2PTQTIY1XZO2Xmfad+f7XgkXxuUrVB6cjPVXef78JyMjA8PFuwyX7gAZj97+T2e9o3d4BWNp84K1NzubTjAB3fH0+fv6N3qP/ir9bW6ozNQCijB8/8zw/TuQ7r4/ndvN5mZgkpPXiis3Pv34+ZPh5t23//79Rwehof/9/NDEgMrOXHvJcrllgpoRN8PFOwy/fzP8+gUlgZI/f/5xcPj/69e/37//AUX+/mXRkN555gsOG2xt/5hZQMwF4r9///75++f3nz8nr75gSms82jfvQnT6zqvXPjC8e/srJQHo9P9fvwNtAHmG4f8zZ6dDc3bIyM2LTNlsbtfM9OPHH3FhtqUz3eXX9H+cOy9ZMB2o6t/Pn0DHMPz/b+2wXGTvPlPGFxdcD+mZyjP8+8MUE6sa7a/xo6Pykn1s4zdzIZ6///8zMGpKM2pKAB0jqy4UE7/msKat6Jw5mafrsxNtWZ6/fjvNLW29qv25pQd///n+5+/fxDDVbcc//P/zx/36m5Ub9zL8+7t66yEROcHK7q5bldMBAgwADcRBCuVLfoEAAAAASUVORK5CYII="
							title="English" alt="English" width="16" height="11" style="width: 16px; height: 11px;"><span
							style="margin-left:0.3em;">English</span></a>
					<ul class="children">
						<li id="menu-item-1604-it"
							class="lang-item lang-item-5 lang-item-it lang-item-first menu-item menu-item-type-custom menu-item-object-custom menu-item-1604-it">
							<a href="index.htm"><img
									src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAE2SURBVHjaYmSYyMDwgwEE/jEw/GF4mvT0HyqQUlX9B5aEIIAAYmH4wlDtWg1SDwT//0lKSv7/D+T9/w+nYmL+//79/88fIPll0yaAAGJhYAGJP/n69O+/v0CAUAcHt2////ULqJpRVhZoA0AAsQCtAZoMVP0HiP7+RlcNBEDVYA0Mv38DNQAEEMj8vwx//wCt/AdC/zEBkgagYoAAYgF6FGj277+///wlpAEoz8AAEEAgDX/BZv/69wuoB48GRrCTAAKICajh9//fv/6CVP/++wu7BrDxQFf/YWAACCCwk0BKf0MQdg1/gBqAPv0L9ANAALEAY+33vz+S3JIgb/z5C45CBkZGRgY4UFICKQUjoJMAAoiRoZSB4RMojkHx/YPhbNVZoM3AOISQQPUK9vaQOIYAgAADAC5Wd4RRwnKfAAAAAElFTkSuQmCC"
									title="Italiano" alt="Italiano" width="16" height="11" style="width: 16px; height: 11px;"><span
									style="margin-left:0.3em;">Italiano</span></a>
						</li>
						<li id="menu-item-1604-en"
							class="lang-item lang-item-9 lang-item-en menu-item menu-item-type-custom menu-item-object-custom menu-item-1604-en">
							<a href="index-2.htm?page_id=3984&amp;lang=en"><img
									src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHzSURBVHjaYkxOP8IAB//+Mfz7w8Dwi4HhP5CcJb/n/7evb16/APL/gRFQDiAAw3JuAgAIBEDQ/iswEERjGzBQLEru97ll0g0+3HvqMn1SpqlqGsZMsZsIe0SICA5gt5a/AGIEarCPtFh+6N/ffwxA9OvP/7//QYwff/6fZahmePeB4dNHhi+fGb59Y4zyvHHmCEAAAW3YDzQYaJJ93a+vX79aVf58//69fvEPlpIfnz59+vDhw7t37968efP3b/SXL59OnjwIEEAsDP+YgY53b2b89++/awvLn98MDi2cVxl+/vl6mituCtBghi9f/v/48e/XL86krj9XzwEEEENy8g6gu22rfn78+NGs5Ofr16+ZC58+fvyYwX8rxOxXr169fPny+fPn1//93bJlBUAAsQADZMEBxj9/GBxb2P/9+S/R8u3vzxuyaX8ZHv3j8/YGms3w8ycQARmi2eE37t4ACCDGR4/uSkrKAS35B3TT////wADOgLOBIaXIyjBlwxKAAGKRXjCB0SOEaeu+/y9fMnz4AHQxCP348R/o+l+//sMZQBNLEvif3AcIIMZbty7Ly6t9ZmXl+fXj/38GoHH/UcGfP79//BBiYHjy9+8/oUkNAAHEwt1V/vI/KBY/QSISFqM/GBg+MzB8A6PfYC5EFiDAABqgW776MP0rAAAAAElFTkSuQmCC"
									title="USA" alt="USA" width="16" height="11" style="width: 16px; height: 11px;"><span
									style="margin-left:0.3em;">USA</span></a>
						</li>
						<li id="menu-item-1604-fr"
							class="lang-item lang-item-13 lang-item-fr menu-item menu-item-type-custom menu-item-object-custom menu-item-1604-fr">
							<a href="index-3.htm?page_id=4041&amp;lang=fr"><img
									src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGzSURBVHjaYiyeepkBBv79+Zfnx/f379+fP38CyT9//jAyMiq5GP77wvDnJ8MfoAIGBoAAYgGqC7STApL///3/9++/pCTv////Qdz/QO4/IMna0vf/z+9/v379//37bUUTQACBNDD8Z/j87fffvyAVX79+/Q8GQDbQeKA9fM+e/Pv18/+vnwzCIkBLAAKQOAY5AIAwCEv4/4PddNUm3ji0QJyxW3rgzE0iLfqDGr2oYuu0l54AYvnz5x9Q6d+/QPQfyAQqAin9B3EOyG1A1UDj//36zfjr1y8GBoAAFI9BDgAwCMIw+P8Ho3GDO6XQ0l4MN8b2kUwYaLszqgKM/KHcDXwBxAJUD3TJ779A8h9Q5D8SAHoARP36+Rfo41+/mcA2AAQQy49ff0Cu//MPpAeI/0FdA1QNYYNVA/3wmwEYVgwMAAHE8uPHH5BqoD1//gJJLADoJKDS378Z//wFhhJAALF8A3rizz8uTmYg788fJkj4QOKREQyYxSWBhjEC/fcXZANAALF8+/anbcHlHz9+ffvx58uPX9KckkCn/gby/wLd8uvHjx96k+cD1UGiGQgAAgwA7q17ZpsMdUQAAAAASUVORK5CYII="
									title="Français" alt="Français" width="16" height="11" style="width: 16px; height: 11px;"><span
									style="margin-left:0.3em;">Français</span></a>
						</li>
						<li id="menu-item-1604-es"
							class="lang-item lang-item-17 lang-item-es menu-item menu-item-type-custom menu-item-object-custom menu-item-1604-es">
							<a href="index-4.htm?page_id=4058&amp;lang=es"><img
									src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFnSURBVHjaYvzPgAD/UNlYEUAAmuTYAAAQhAEYqF/zFbe50RZ1cMmS9TLi0pJLRjZohAMTGFUN9HdnHgEE1sDw//+Tp0ClINW/f0NIKPoFJH/9//ULyGaUlQXaABBALAx/Gf4zAt31F4i+ffj3/cN/XrFfzOx//v///f//LzACM/79ZmD8/e8TA0AAMYHdDVT958vXP38nMDB0s3x94/Tj5y+YahhiAKLfQKUAAcQEdtJfoDHMF2L+vPzDmFXLelf551tGFOOhev4A/QgQQExgHwAd8IdFT/Wz6j+GhlpmXSOW/2z///8Eq/sJ18Dw/zdQA0AAMQExxJjjdy9x2/76EfLz4MXdP/i+wsyGkkA3Aw3984cBIIAYfzIwMKel/bt3jwEaLNAwgZIQxp/fDH/+MqqovL14ESCAWICeZvr9h0FSEhSgwBgAygFDEMT+wwAhgQgc4kAEVAwQQIxfUSMSTxxDAECAAQAJWke8v4u1tAAAAABJRU5ErkJggg=="
									title="Español" alt="Español" width="16" height="11" style="width: 16px; height: 11px;"><span
									style="margin-left:0.3em;">Español</span></a>
						</li>
						<li id="menu-item-1604-de"
							class="lang-item lang-item-21 lang-item-de menu-item menu-item-type-custom menu-item-object-custom menu-item-1604-de">
							<a href="index-5.htm?page_id=4066&amp;lang=de"><img
									src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGzSURBVHjaYvTxcWb4+53h3z8GZpZff/79+v3n/7/fDAz/GHAAgABi+f37e3FxOZD1Dwz+/v3z9y+E/AMFv3//+Qumfv9et241QACxMDExAVWfOHkJJAEW/gUEP0EQDn78+AHE/gFOQJUAAcQiy8Ag8O+fLFj1n1+/QDp+/gQioK7fP378+vkDqOH39x9A/RJ/gE5lAAhAYhzcAACCQBDkgRXRjP034R0IaDTZTFZn0DItot37S94KLOINerEcI7aKHAHE8v/3r/9//zIA1f36/R+o4tevf1ANYNVA9P07RD9IJQMDQACxADHD3z8Ig4GMHz+AqqHagKp//fwLVA0U//v7LwMDQACx/LZiYFD7/5/53/+///79BqK/EMZ/UPACSYa/v/8DyX9A0oTxx2EGgABi+a/H8F/m339BoCoQ+g8kgRaCQvgPJJiBYmAuw39hxn+uDAABxMLwi+E/0PusRkwMvxhBGoDkH4b/v/+D2EDyz///QB1/QLb8+sP0lQEggFh+vGXYM2/SP6A2Zoaf30Ex/J+PgekHwz9gQDAz/P0FYrAyMfz7wcDAzPDtFwNAgAEAd3SIyRitX1gAAAAASUVORK5CYII="
									title="Deutsch" alt="Deutsch" width="16" height="11" style="width: 16px; height: 11px;"><span
									style="margin-left:0.3em;">Deutsch</span></a>
						</li>
					</ul>
				</li>
			</ul>
		</div><!-- inner -->
	</div><!-- #mobile-menu -->
	<!--googleoff: all-->
	<div id="cookie-law-info-again" style="display:none;" data-nosnippet="true"><span id="cookie_hdr_showagain">Privacy &
			Cookies Policy</span></div>
	<div class="cli-modal" data-nosnippet="true" id="cliSettingsPopup" tabindex="-1" role="dialog"
		aria-labelledby="cliSettingsPopup" aria-hidden="true">
		<div class="cli-modal-dialog" role="document">
			<div class="cli-modal-content cli-bar-popup">
				<button type="button" class="cli-modal-close" id="cliModalClose">
					<svg class="" viewbox="0 0 24 24">
						<path
							d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z">
						</path>
						<path d="M0 0h24v24h-24z" fill="none"></path>
					</svg>
					<span class="wt-cli-sr-only">Close</span>
				</button>
				<div class="cli-modal-body">
					<div class="cli-container-fluid cli-tab-container">
						<div class="cli-row">
							<div class="cli-col-12 cli-align-items-stretch cli-px-0">
								<div class="cli-privacy-overview">
									<h4>Privacy Overview</h4>
									<div class="cli-privacy-content">
										<div class="cli-privacy-content-text">This website uses cookies to improve your experience while you
											navigate through the website. Out of these, the cookies that are categorized as necessary are
											stored on your browser as they are essential for the working of basic functionalities of the
											website. We also use third-party cookies that help us analyze and understand how you use this
											website. These cookies will be stored in your browser only with your consent. You also have the
											option to opt-out of these cookies. But opting out of some of these cookies may affect your
											browsing experience.</div>
									</div>
									<a class="cli-privacy-readmore" data-readmore-text="Show more" data-readless-text="Show less"></a>
								</div>
							</div>
							<div class="cli-col-12 cli-align-items-stretch cli-px-0 cli-tab-section-container">

								<div class="cli-tab-section">
									<div class="cli-tab-header">
										<a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="necessary"
											data-toggle="cli-toggle-tab">
											Necessary </a>
										<div class="wt-cli-necessary-checkbox">
											<input type="checkbox" class="cli-user-preference-checkbox" id="wt-cli-checkbox-necessary"
												data-id="checkbox-necessary" checked="checked">
											<label class="form-check-label" for="wt-cli-checkbox-necessary">Necessary</label>
										</div>
										<span class="cli-necessary-caption">Always Enabled</span>
									</div>
									<div class="cli-tab-content">
										<div class="cli-tab-pane cli-fade" data-id="necessary">
											<p>Necessary cookies are absolutely essential for the website to function properly. This category
												only includes cookies that ensures basic functionalities and security features of the website.
												These cookies do not store any personal information.</p>
										</div>
									</div>
								</div>

								<div class="cli-tab-section">
									<div class="cli-tab-header">
										<a role="button" tabindex="0" class="cli-nav-link cli-settings-mobile" data-target="non-necessary"
											data-toggle="cli-toggle-tab">
											Non-necessary </a>
										<div class="cli-switch">
											<input type="checkbox" id="wt-cli-checkbox-non-necessary" class="cli-user-preference-checkbox"
												data-id="checkbox-non-necessary" checked='checked'>
											<label for="wt-cli-checkbox-non-necessary" class="cli-slider" data-cli-enable="Enabled"
												data-cli-disable="Disabled"><span class="wt-cli-sr-only">Non-necessary</span></label>
										</div>
									</div>
									<div class="cli-tab-content">
										<div class="cli-tab-pane cli-fade" data-id="non-necessary">
											<p>Any cookies that may not be particularly necessary for the website to function and is used
												specifically to collect user personal data via analytics, ads, other embedded contents are
												termed as non-necessary cookies. It is mandatory to procure user consent prior to running these
												cookies on your website.</p>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
	<div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
	<!--googleon: all-->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async="" src="gtag/js.js?id=UA-55461567-24"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() { dataLayer.push(arguments); }
		gtag('js', new Date());

		gtag('config', 'UA-55461567-24');
	</script>
	<link rel='stylesheet' id='elementor-post-1548-css'
		href='wp-content/uploads/elementor/css/post-1548.css?ver=1620126438' type='text/css' media='all'>
	<link rel='stylesheet' id='element-pack-font-css'
		href='wp-content/plugins/bdthemes-element-pack/assets/css/element-pack-font.css?ver=4.6.1' type='text/css'
		media='all'>
	<link rel='stylesheet' id='ep-navbar-css'
		href='wp-content/plugins/bdthemes-element-pack/assets/css/ep-navbar.css?ver=4.6.1' type='text/css' media='all'>
	<link rel='stylesheet' id='ep-custom-carousel-css'
		href='wp-content/plugins/bdthemes-element-pack/assets/css/ep-custom-carousel.css?ver=4.6.1' type='text/css'
		media='all'>
	<link rel='stylesheet' id='ep-advanced-icon-box-css'
		href='wp-content/plugins/bdthemes-element-pack/assets/css/ep-advanced-icon-box.css?ver=4.6.1' type='text/css'
		media='all'>
	<link rel='stylesheet' id='ep-advanced-button-css'
		href='wp-content/plugins/bdthemes-element-pack/assets/css/ep-advanced-button.css?ver=4.6.1' type='text/css'
		media='all'>
	<link rel='stylesheet' id='cf-render-css'
		href='wp-content/plugins/caldera-forms/clients/render/build/style.min.css?ver=1.9.2' type='text/css' media='all'>
	<script type='text/javascript' src='wp-content/plugins/modal-for-elementor/js/bootstrap.js'
		id='bootstrap-js'></script>
	<script type='text/javascript' src='wp-content/plugins/modal-for-elementor/js/popup.js'
		id='modal-popup-js-js'></script>
	<script type='text/javascript' src='wp-includes/js/hoverIntent.min.js?ver=1.8.1' id='hoverIntent-js'></script>
	<script type='text/javascript' id='flatsome-js-js-extra'>
		/* <![CDATA[ */
		var flatsomeVars = { "ajaxurl": "https:\/\/www.mirsmartone.com\/wp-admin\/admin-ajax.php", "rtl": "", "sticky_height": "30", "user": { "can_edit_pages": false } };
/* ]]> */
	</script>
	<script type='text/javascript' src='wp-content/themes/flatsome/assets/js/flatsome.js?ver=3.6.2'
		id='flatsome-js-js'></script>
	<script type='text/javascript' src='wp-includes/js/wp-embed.min.js?ver=5.7.1' id='wp-embed-js'></script>

	<script type='text/javascript' src='wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=4.6.1'
		id='bdt-uikit-js'></script>
	<script type='text/javascript'
		src='wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit-icons.min.js?ver=3.0.3'
		id='bdt-uikit-icons-js'></script>
	<script type='text/javascript' src='wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=2.9.8'
		id='elementor-frontend-modules-js'></script>
	<script type='text/javascript' src='wp-includes/js/jquery/ui/core.min.js?ver=1.12.1' id='jquery-ui-core-js'></script>
	<script type='text/javascript' src='wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.7.6'
		id='elementor-dialog-js'></script>
	<script type='text/javascript' src='wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'
		id='elementor-waypoints-js'></script>
	<script type='text/javascript' src='wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6'
		id='swiper-js'></script>
	<script type='text/javascript' src='wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=2.9.8'
		id='share-link-js'></script>
	<script type='text/javascript' id='elementor-frontend-js-before'>
		var elementorFrontendConfig = { "environmentMode": { "edit": false, "wpPreview": false }, "i18n": { "shareOnFacebook": "Share on Facebook", "shareOnTwitter": "Share on Twitter", "pinIt": "Pin it", "downloadImage": "Download image" }, "is_rtl": false, "breakpoints": { "xs": 0, "sm": 480, "md": 768, "lg": 1025, "xl": 1440, "xxl": 1600 }, "version": "2.9.8", "urls": { "assets": "https:\/\/www.mirsmartone.com\/wp-content\/plugins\/elementor\/assets\/" }, "settings": { "page": [], "general": { "elementor_global_image_lightbox": "yes", "elementor_lightbox_enable_counter": "yes", "elementor_lightbox_enable_fullscreen": "yes", "elementor_lightbox_enable_zoom": "yes", "elementor_lightbox_enable_share": "yes", "elementor_lightbox_title_src": "title", "elementor_lightbox_description_src": "description" }, "editorPreferences": [] }, "post": { "id": 4049, "title": "MIR%20SmartOne", "excerpt": "", "featuredImage": false } };
	</script>
	<script type='text/javascript' src='wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.9.8'
		id='elementor-frontend-js'></script>
	<script type='text/javascript'
		src='wp-content/plugins/bdthemes-element-pack/assets/js/element-pack-site.min.js?ver=4.6.1'
		id='element-pack-site-js'></script>
	<script type='text/javascript'
		src='wp-content/plugins/bdthemes-element-pack/assets/js/widgets/ep-custom-carousel.min.js?ver=4.6.1'
		id='ep-custom-carousel-js'></script>
	<script type='text/javascript'
		src='wp-content/plugins/bdthemes-element-pack/assets/js/widgets/ep-advanced-icon-box.min.js?ver=4.6.1'
		id='ep-advanced-icon-box-js'></script>

	<script type='text/javascript' src='wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=7.4.4'
		id='wp-polyfill-js'></script>

	<script type='text/javascript' src='wp-includes/js/dist/vendor/react.min.js?ver=16.13.1' id='react-js'></script>
	<script type='text/javascript' src='wp-includes/js/dist/vendor/react-dom.min.js?ver=16.13.1'
		id='react-dom-js'></script>
	<script type='text/javascript' src='wp-includes/js/dist/dom-ready.min.js?ver=eb19f7980f0268577acb5c2da5457de3'
		id='wp-dom-ready-js'></script>

	<script type='text/javascript'
		src='wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min.js?ver=0.2.1'
		id='jquery-numerator-js'></script>

	<script src="https://unpkg.com/imask"></script>
	<script>
		let element = document.getElementById('fld_6827714_1');
		let maskOptions = {
			mask: '+00 000-000-000'
		};
		let mask = IMask(element, maskOptions);
	</script>
	<script type='text/javascript' src='./submitForm.js'></script>
	<?php 
		echo $send;
		
		if($send) {
	?>
	<script type="text/javascript">
		let alertSuccess = document.getElementById("alert-success");
		alertSuccess.hidden = false;
		setTimeout(function hiddenAlert () {
			alertSuccess.hidden = true;
			window.location.replace("http://spirometer.pl");
		}, 4000);
	</script>
	<?php 
		unset($send);
		var_dump($send);
		}
	?>
</body>

</html>